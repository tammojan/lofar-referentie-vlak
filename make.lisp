(require 'asdf)
(require 'sb-posix)

#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))



(mapcar #'ql:quickload '("cl-utilities"
                     "cffi"
                     "cl-ppcre"
                     "cl-pdf"))


(push (merge-pathnames #P".asdf-install-dir/systems/" (user-homedir-pathname))
      asdf:*central-registry*)
(push "/usr/lib/sbcl/site-systems/" asdf:*central-registry*)
(push (merge-pathnames "Software/common-lisp/systems/" (user-homedir-pathname))
      asdf:*central-registry*)

(asdf:oos 'asdf:load-op :reference-plane)

(in-package :lofar-reference-plane)

(sb-ext:save-lisp-and-die 
 "statcor"
 :toplevel   #'lofar-reference-plane::main
 :executable t)
