#!/bin/bash

solution=$1
lba_stakes_pqr=$2
prefix=$3

lba_pqr=$prefix-lba-pqr.csv
lba_etrs=$prefix-lba-etrs.csv

echo "NAME;STATION-P;STATION-Q;STATION-R" > $lba_pqr
cat $lba_stakes_pqr|grep -e'^M'|sed -e's/M//g'|sed -e's/,/ /g'| \
    sort -n -k 1| \
    awk '// {print "L" $1 "," $2+0.06 "," $3+0.06 ",""0.000"}' >>$lba_pqr 

statcor pqr-to-etrs -o $lba_etrs $solution $lba_pqr