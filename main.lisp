#||

\section{Command line interface}

||#
(in-package :lofar-reference-plane)
#||

Here I discuss the main program, which implements the command line
interface of the application.  There are several bits of
infrastructure in this file:

\begin{itemize}
\item \cl{print-help} looks up help strings from the \cl{*help*} parameter
  in the file help-text.lisp.
\item \cl{print-condition} nicely formats error messages.
\item Then there is a number of conditions that may be raised whenever
  there are problems with the user input.
\item \cl{main} is the function that dispatches control based on the
  first command line argument and handles any erros and conditions that
  were not handled earlier.
\end{itemize}


\subsection{Conditions}

||#
(define-format-condition command-missing-error (error)
  ()
  ("Please supply a command."))

(define-format-condition invalid-command-error (error)
  (command)
  ("\"~a\" is not a valid command." (command invalid-command-error)))

(define-format-condition no-help-error (error)
  (help-item)
  ("No help defined for command \"~a\"~%~%" (help-item no-help-error)))
#||

\subsection{Feedback to the user}

||#
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *version*
    (apply #'format nil
           "~4d-~2,'0d-~2,'0d ~2,'0d:~2,'0d:~2,'0d UTC"
           (reverse (subseq (multiple-value-list
                             (decode-universal-time
                              (get-universal-time) 0))
                            0 6)))))

(defun print-version ()
  (format *standard-output*
          "~&statcor version ~a by M.A. Brentjens <brentjens@astron.nl>~%"
          *version*))

(defun print-help (which-help)
  (let ((help-text (second (assoc which-help *help*))))
    (unless help-text (error 'no-help-error :help-item which-help))
    (format *standard-output* "~&~a~%" help-text)))

(defun print-condition (condition)
  (format *error-output*
          "~&~%statcor: error: ~a~%~%Use \"statcor help\" for more information.~%"
          condition))
#||

\subsection{Implementation of the commands}

The \cl{defcommand} macro is used to define command line callable
commands. Specifying a name for the local \cl{argv} variable is
required. First, however, we need a couple of support functions. The
most important ones read and write the results of the \cl{fit-plane}
command in such a way, that it can be used by the etrs-to-pqr and
pqr-to-etrs commands.

||#
(defun write-fit-plane-result (fit-plane-result stream)
  (let* ((discarded     (getf fit-plane-result :discarded))
         (used          (getf fit-plane-result :used))
         (solution-info (getf fit-plane-result :solution-info))
         (normal-plane-solution (getf fit-plane-result :normal-plane-solution))
         (average-position (average-position used)))
    
    (format stream "~&(discarded-points
    ~s)
~&(reduced-chi-squared ~f)
~&(normal-vector               (~{~9,7f~^ ~}))
~&(normal-vector-uncertainties (~{~9,7f~^ ~}))
~&(normal-vector-direction-uncertainties-arcsec :max ~5,2f :rms ~5,2f :avg ~5,2f)
~&(reference-point :etrs (~{~,3f~^ ~})
                 :station-pqr (0.0 0.0 0.0))
~&(station-pqr-to-etrs-matrix
    ~s)
"
            (mapcar #'as-csv-string discarded)
            (reduced-chi-squared solution-info)
            (to-list (normal-vector normal-plane-solution))
            (to-list (uncertainties normal-plane-solution))
            (arcsec-from-rad (reduce #'max (uncertainties normal-plane-solution)))
            (arcsec-from-rad (rms          (uncertainties normal-plane-solution)))
            (arcsec-from-rad (average      (uncertainties normal-plane-solution)))
            (list (aref average-position 0)
                  (aref average-position 1)
                  (+ (* (coef-a solution-info) (aref average-position 0))
                     (* (coef-b solution-info) (aref average-position 1))
                     (coef-c solution-info)))
            (station-to-etrs89-matrix (normal-vector normal-plane-solution)))))

(defun read-fit-plane-output (stream)
  (let ((*read-default-float-format* 'double-float))
    (read-from-string
     (format nil "(~{~a~%~})" (read-lines stream)))))
#||

Here follow the commands themselves.

||#
(defcommand help (argv)
  (print-help
   (if argv
       (intern (string-upcase (first argv)) :lofar-reference-plane)
       'help)))


(defcommand version (argv)
  (print-version))


(defcommand fit-plane (argv)
  (let ((parser-args nil)
        (output-file nil)
        (stream-parse-function #'parse-stream)
        (file-parse-function #'parse-file)
        (*read-default-float-format* 'double-float))
    (let ((input-file
           ;; PARSE THE COMMAND LINE
           (parse-command-line argv

             (("-u" "--uncertainty") (uncertainty)
              (setf uncertainty (read-from-string uncertainty))
              (unless (numberp uncertainty)
                (error "Option --uncertainty needs a number argument."))
              (setf parser-args (list* :uncertainty uncertainty parser-args)))
             
             (("-o" "--output") (output-file-name)
              (setf output-file output-file-name))

             (("-l" "--input-lon-lat") ()
              (setf stream-parse-function #'parse-azimuth-etrs-lon-lat
                    file-parse-function   #'parse-azimuth-etrs-lon-lat-file)))))

      (unless (<= (length input-file) 1)
        (error "Expecting at most one input file."))

      (let ((fit-results
             (fit-plane (if (first input-file)
                            (apply file-parse-function
                                   (append input-file parser-args))
                            (apply stream-parse-function
                                   (list* *standard-input* parser-args))))))
        (if output-file
            (with-open-file (stream output-file :direction :output :if-exists :supersede)
              (write-fit-plane-result fit-results stream)
              (write-fit-plane-result fit-results *error-output*))
            (write-fit-plane-result fit-results *standard-output*))
      ))))


(defcommand etrs-to-pqr (argv)
  (let* ((output-file nil)
         (stream-parse-function #'parse-stream)
         (file-parse-function #'parse-file)
         (*read-default-float-format* 'double-float)
         (arguments
          (parse-command-line argv
            (("-o" "--output") (output-file-name)
             (setf output-file output-file-name))
            (("-l" "--input-lon-lat") ()
             (setf stream-parse-function #'parse-azimuth-etrs-lon-lat
                   file-parse-function   #'parse-azimuth-etrs-lon-lat-file)))))
    
    (unless (> (length arguments) 0)
      (error "Please specify a plane specification file."))
    (unless (< (length arguments) 3)
      (error "Please specify at most one input file."))

    (let* ((plane-spec-file (first arguments))
           (positions (if (rest arguments)
                          (funcall file-parse-function (first (rest arguments)))
                          (funcall stream-parse-function *standard-input*)))
           (plane-spec (with-open-file (stream plane-spec-file :direction :input)
                         (read-fit-plane-output stream)))
           (name-pqr
            (mapcar #'(lambda (position)
                        (list* (position-id position)
                               (etrs-x position) 
                               (etrs-y position) 
                               (etrs-z position)
                               (to-list
                                (transform-etrs89-to-station 
                                 (vector (etrs-x position) 
                                         (etrs-y position) 
                                         (etrs-z position))
                                 :normal-vector (to-vector 
                                                 (second 
                                                  (assoc 'normal-vector plane-spec)))
                                 :station-0 (to-vector
                                             (getf (rest (assoc 'reference-point plane-spec))
                                                   :station-pqr))
                                 :etrs89-0  (to-vector
                                             (getf (rest (assoc 'reference-point plane-spec))
                                                   :etrs))))))
                    positions))
           (header "NAME;ETRS-X;ETRS-Y;ETRS-Z;STATION-P;STATION-Q;STATION-R"))
      (if output-file
          (with-open-file (stream output-file :direction :output :if-exists :supersede)
            (format stream
                    "~&~a~%~{~{~a;~,3f;~,3f;~,3f;~,3f;~,3f;~,3f~%~}~}"
                    header
                    name-pqr))
          (format *standard-output*
                  "~&~a~%~{~{~a;~,3f;~,3f;~,3f;~,3f;~,3f;~,3f~%~}~}"
                  header
                  name-pqr)))))


(defcommand pqr-to-etrs (argv)
  (let* ((output-file nil)
         (format-name 'csv)
         (base-number 1001)
         (*read-default-float-format* 'double-float)
         (arguments
          (parse-command-line argv
            (("-o" "--output") (output-file-name)
             (setf output-file output-file-name))
            (("-f" "--format") (format)
             (setf format-name (read-from-string format)))
            (("-b" "--base-number") (nr)
             (setf base-number (read-from-string nr))))))
    
    (unless (> (length arguments) 0)
      (error "Please specify a plane specification file."))
    (unless (< (length arguments) 3)
      (error "Please specify at most one input file."))
    
    (let* ((plane-spec-file (first arguments))
           (positions (remove-if 
                       #'(lambda (x)
                           (every #'symbolp x))
                       (mapcar #'parse-line
                               (if (rest arguments)
                                   (with-open-file (stream (first (rest arguments))
                                                           :direction :input)
                                     (read-lines stream))
                                   (read-lines *standard-input*)))))
           (plane-spec (with-open-file (stream plane-spec-file :direction :input)
                         (read-fit-plane-output stream)))
           (name-etrs-pqr
            (mapcar #'(lambda (position)
                        (list* (first position)
                               (append
                                (to-list
                                 (transform-station-to-etrs89
                                  (to-vector (subseq position 1 4))
                                  :normal-vector (to-vector 
                                                  (second 
                                                   (assoc 'normal-vector plane-spec)))
                                  :station-0 (to-vector
                                              (getf (rest (assoc 'reference-point plane-spec))
                                                    :station-pqr))
                                  :etrs89-0  (to-vector
                                              (getf (rest (assoc 'reference-point plane-spec))
                                                    :etrs))))
                                (subseq position 1 4))))
                    positions))
           (header "NAME;ETRS-X;ETRS-Y;ETRS-Z;STATION-P;STATION-Q;STATION-R"))
      (flet ((write-output (stream header name-etrs-pqr)
               (ecase format-name
                 (csv (format stream
                              "~&~a~%~{~{~a;~,3f;~,3f;~,3f;~,3f;~,3f;~,3f~%~}~}"
                              header
                              name-etrs-pqr))
                 (totalstation 
                  (format stream
                          "~&~{~{~a,~2,'0d.~2,'0d~5,'0d,~2,'0d.~2,'0d~5,'0d,~,3f,~a~a~a~}~}"
                          (loop :for id :from base-number
                             :for (name x y z p q r) :in name-etrs-pqr
                             :for (lon lat h) = (geographic-from-xyz (list x y z))
                             :collect (append (list name)
                                              (subseq  (sdms-from-rad lat :decimals 3)1 3)
                                              (list 
                                               (floor (+ (* 1000 (first(last (sdms-from-rad lat :decimals 3)))) 0.5d0)))
                                              (subseq (sdms-from-rad lon :decimals 3) 1 3)
                                              (list 
                                               (floor (+ (* 1000 (first(last (sdms-from-rad lon :decimals 3)))) 0.5d0)))
                                              (list h name #\Return #\Linefeed))))))))
                    
        (if output-file
            (with-open-file (stream output-file :direction :output :if-exists :supersede)
              (write-output stream header name-etrs-pqr))
            (write-output *standard-output* header name-etrs-pqr)
            )))))




(defcommand angle (argv)
  (unless (= (length argv) 2)
    (error "Please specify two plane specification files containing one
\(normal-vector (... ... ...)) entry each."))
  (destructuring-bind (file1 file2) argv
    (with-open-file (stream1 file1 :direction :input)
      (with-open-file (stream2 file2 :direction :input)
        (let ((angle
               (sdms-from-rad
                (acos
                 (inner-product (to-vector
                                 (first (rest (assoc 'normal-vector
                                                     (read-fit-plane-output stream1)))))
                                (to-vector
                                 (first (rest (assoc 'normal-vector
                                                     (read-fit-plane-output stream2))))))))))
          (format *standard-output*
                  "Angle between normal vectors: ~a~2,'0d~a ~2,'0d' ~5,2,,,'0f\".~%"
                  (first angle)
                  (second angle)
                  #\DEGREE_SIGN
                  (third angle)
                  (fourth angle)
                  ))))))

(defcommand project (argv)
  (let ((*read-default-float-format* 'double-float))
    (let* ((normal    (unit-vector *nap-normal-vector*))
           (reference nil)
           (arguments
            (parse-command-line argv
              (("-n" "--normal") (normal-text)
               (setf normal
                     (apply #'vector
                            (read-from-string
                             (concatenate 'string "(" normal-text ")")))))
              (("-r" "--reference") (reference-text)
               (setf reference
                     (apply #'vector
                            (read-from-string
                             (concatenate 'string "(" reference-text ")"))))))))

      (unless (and (= 3 (length normal))
                   (every #'numberp normal))
        (error "Normal vector ~a must contain 3 numbers." normal))
      (unless reference (error "A reference point is required."))
      (unless (and (= 3 (length reference))
                   (every #'numberp reference))
        (error "Reference point ~a must contain 3 numbers." reference))

      (format t "~{~,3f~^ ~}~%"
              (to-list (project-point
                        (case (length arguments)
                          ((1 3) (apply #'vector
                                        (read-from-string (format nil "(~{~a~^ ~})" arguments))))
                          (otherwise
                           (error "Specify the point to project as \"<X> <Y> <Z>\".")))
                        :point-in-plane reference
                        :normal-vector  normal))))))
#||

\subsection{The main routine}

||#
(defun main ()
  (let ((*package* (find-package :lofar-reference-plane)))
    ;; Rebinding *package* is necessary to read all symbols in this package
    (handler-case
        (let ((argv (rest sb-ext:*posix-argv*)))
          (if (null argv)
              (error 'command-missing-error)
              (let ((command (intern (string-upcase (first argv)) :lofar-reference-plane)))
                (if (valid-command-p command)
                    (execute-command command (rest argv))
                    (error 'invalid-command-error :command command)))))
      (condition (condition)
        (print-condition condition)
        -1))))