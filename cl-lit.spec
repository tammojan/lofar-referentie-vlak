nil ;;"reference-plane-report.tex" ;; name of the main document without path or extension 
                  ;; Example: "manual.tex"
                  ;; This generates a file called "manual.tex".
:output-dir "doc/";; Relative path in which all TeX files should be dumped
                  ;; Default: "./". The slash is optional.
:preamble "doc/preamble.tex" ;; This file should include all usepackage, 
                  ;; title, and author declarations that are needed.
                  ;; The contents will be copied verbatim into the output file.
:files (("reference-plane.lisp" "Reference plane")      ;; First the appropriate TeX documentation is 
                               ;; extracted into the :output-dir. This is 
                               ;; subsequently input into the main file.
        :appendix              ;; Generates an appendix command
        ;("package.lisp"            "Package")
        ("csv-txt-input.lisp"       "Input through CSV and TXT files")
        ("utilities.lisp"           "Utilities")
        ("main.lisp"                "Command line interface")
        ("help-text.lisp"           "On-line help texts")
        ;("unit-test-execution.lisp" "Unit tests")
        :index)
