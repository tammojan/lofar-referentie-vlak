from pylab import *
from numpy import *
import csv
import scipy.interpolate.rbf as rbf


def column(dict_list, field, conversion_function=None):
    if conversion_function:
        return [conversion_function(x[field]) for x in dict_list]
    else:
        return [x[field] for x in dict_list]

def read_pqr(csvfilename, delimiter=';'):
    csvreader=csv.reader(open(csvfilename),delimiter=delimiter)
    header=[]
    data=[]
    for line in csvreader:
        if line[0] == 'NAME':
            header = line
        else:
            data += [dict(zip(header,line))]
            pass
        pass
    return (column(data, 'NAME'),
            column(data, 'STATION-P', float),
            column(data, 'STATION-Q', float),
            column(data, 'STATION-R', float))




def plot_height_map_stats(p,q,r, plot_title, resolution_m=1.0,vmax=0.04, standard_deviations=[],plot_points=True,plot_contours=True, antenna_positions=[], mean_of_good_points=None, contour_interval=0.1):
    fig=figure(figsize=(12,4),dpi=300)
    clf()
    rbf_fn = rbf.Rbf(p,q,r,function='linear')
    x,y= meshgrid(arange(min(p)-2, max(p)+2, resolution_m),
                  arange(min(q)-2, max(q)+2, resolution_m))
    z = rbf_fn(x,y)
    contour_levels=contour_interval*arange(-20,20.01)
    subplot(121)
    cla()
    title(plot_title,size=30)
    pcolor(x,y,z,vmin=-vmax,vmax=vmax)
    axis('equal')
    colorbar()
    if plot_points:
        scatter(p,q,marker='+',s=4,c='black')
        pass
    if plot_contours:
        contour(x,y,z,contour_levels[contour_levels != 0.0],colors='black')
        pass
    if antenna_positions:
        names,ap,aq,ar = antenna_positions
        scatter(ap, aq, marker='o',s=10,c='white')
        pass

    subplot(122)
    cla()
    title_text = 'Histogram of deviations'
    if mean_of_good_points is not None:
        title_text += ' (mean %5.3f)' % mean_of_good_points
    title(title_text)
    n,bins,patches=hist(r,bins=0.01*arange(-10.0,10.1,0.5)+0.0025,normed=1)
    for sigma in sorted(standard_deviations):
        plot(bins,normpdf(bins, 0.0, sigma), label='stddev='+str(sigma),linewidth=2)
        pass
    legend()
    return fig
    

def plot_height_map(p,q,r, plot_title, resolution_m=1.0,vmax=0.04, standard_deviations=[],plot_points=True,plot_contours=True, antenna_positions=[], contour_interval=0.1):

    fig=figure(figsize=(12,8),dpi=300)
    clf()
    rbf_fn = rbf.Rbf(p,q,r,function='linear')
    x,y= meshgrid(arange(min(p)-2, max(p)+2, resolution_m),
                  arange(min(q)-2, max(q)+2, resolution_m))
    z = rbf_fn(x,y)
    contour_levels=contour_interval*arange(-20,20.01)
    cla()
    title(plot_title,size=30)
    pcolor(x,y,z,vmin=-vmax,vmax=vmax)
    axis('equal')
    colorbar()
    if plot_points:
        scatter(p,q,marker='+',s=4,c='black')
        pass
    if plot_contours:
        contour(x,y,z,contour_levels[contour_levels != 0.0],colors='black')
        pass
    if antenna_positions:
        names,ap,aq,ar = antenna_positions
        scatter(ap, aq, marker='o',s=10,c='white')
        pass
    pass
