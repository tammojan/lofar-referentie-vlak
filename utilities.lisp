(in-package :lofar-reference-plane)

#||
\section{Utility functions}

\subsection{Lisp image management}

Reload the package. You can call this function from the
Read-Evaluate-Print-Loop (REPL) whenever you want to recompile any new
functions and run the test suite.

||#
(defun reload ()
  (asdf:oos 'asdf:load-op :reference-plane))
#||

Removing a function definition and its associated unit test from the
Lisp image. This is particularly useful after renaming a function when
you want to remove the function with the old name.

||#
(defun remove-function (name)
  (fmakunbound name)
  (fmakunbound (intern (format nil "TEST-~a" (symbol-name name))))
  (unit-test::remove-test (intern (format nil "TEST-~a" (symbol-name name)))))
#||

How long is the current development image running? Just because I
sometimes am curious.

||#
(defun uptime ()
  (multiple-value-bind (h s) 
      (floor (/ (get-internal-real-time)
                internal-time-units-per-second)
             3600)
    (format t "Image uptime: ~{~d days ~2d~} hours ~2d minutes~%"
            (multiple-value-list (floor h 24))
            (floor s 60))))
#||

\subsection{General macros}

The macro \cl{lambda-key} is a very general macro. It can be used
when you want to allow people to specify \cl{key} functions for
certain variables analogous to for example the \cl{reduce}
function. It is very useful if one wants to use ``nested'' key
functions, such as in the first example below. Specifying a possible
key function for a variable is optional, as is shown in the last
example. Usage examples:
{\small\begin{lstlisting}
(defun rms (sequence &key key)
  (sqrt
    (average sequence :key (lambda-key ((x key)) (square x)))))

(defun one-plus-seq3 (sequence1 sequence2 sequence3&key key1 key2)
  (mapcar (lambda-key ((x key1) (y key2) z)
            (+ (square x) (square y) z))
          sequence1 sequence2 sequence3))
\end{lstlisting}}

If no keys are specified, the \cl{lambda-key} macro reduces to a
simple \cl{lambda} call.

||#
(defmacro lambda-key (var-key-pairs &body body)
  (let* ((lambda-list (mapcar #'(lambda (pair)
                                  (if (listp pair) (first pair) pair))
                              var-key-pairs))
         (key-list    (remove-if #'null
                                 (mapcar #'(lambda (pair)
                                             (when (listp pair)
                                               (second pair)))
                                         var-key-pairs)))
         (key-var-names (mapcar (lambda (x)
                                  (declare (ignore x))(gensym))
                                key-list)))
    (if key-list
        `(let ,(mapcar #'(lambda (kvn key) `(,kvn ,key)) key-var-names key-list)
           (if ,(if (= 1 (length key-var-names))
                    (first key-var-names)
                    `(some #'identity (list ,@key-var-names)))
             (lambda ,lambda-list
               (let ,(mapcar #'(lambda (var kvn) `(,var (funcall ,kvn ,var)))
                             (mapcar #'first (remove-if #'atom var-key-pairs))
                             key-var-names)
                 ,@body))
             (lambda ,lambda-list ,@body)))
        `(lambda ,lambda-list ,@body))))
#||

\subsection{List manipulation}

Merging a collection of lists like a zipper. See the test cases for
examples. It is surprisingly useful.

||#
(deftest test-zip ()
  (assert-equal (zip '(:a :b :c) '(1 2 3 4 5))
                '(:a 1 :b 2 :c 3))
  (assert-equal (zip '(:a :b :c) '(1 2))
                '(:a 1 :b 2))
  (assert-equal (zip '(:a :b :c) '(1 2) '(3 4))
                '(:a 1 3 :b 2 4)))

(defun zip (&rest lists)
  (apply #'mapcan #'list lists))
#||



\subsection{Arithmetic}

Some simple arithmetic tools to improve the readability of some code
sections.

||#
(deftest test-square ()
  (assert-equal (square -3) 9)
  (assert-equal (square  5) 25))

(defun square (x) (* x x))

(deftest test-cube ()
  (assert-equal (cube -3) -27)
  (assert-equal (cube  5) 125))

(defun cube (x) (* x x x))
#||

\subsection{Statistics}

Compute the average of a sequence of numbers.

||#
(deftest test-average ()
  (assert-eql (average #(2 3 4 5 6)) 4)
  (assert-eql (average '(-1 0 4)) 1)
  (assert-eql (average '(-1 0 4) :key (lambda (x) (* 2 x))) 2)
  (assert-error (average #()) error))

(defun average (sequence &key key)
  (let ((n (length sequence)))
    (if (zerop n)
      (error "Sequence is empty.")
      (/ (reduce #'+ sequence :key key) n))))
#||

Compute the root of the mean square (RMS) value of a sequence. The
\cl{:key} argument works the same as in the standard function
\cl{reduce}.

||#
(deftest test-rms ()
  (assert-error (rms '()) error)
  (assert-approx= (rms #(3 4 5)) (sqrt 50/3))
  (assert-approx= (rms #(3 4 5) :key (lambda (x) (* 2 x))) (sqrt 200/3)))
  
(defun rms (sequence &key key)
  (sqrt
   (average sequence :key (lambda-key ((x key)) (square x)))))
#||

Compute the standard deviation of the numbers in an array by computing
\begin{equation}
  \sigma^2 = \left(N-1\right)^{-1}\sum_i=1^N \left(a_i -\langle a \rangle\right)^2
\end{equation}

||#
(deftest test-standard-deviation ()
  (assert-approx= (standard-deviation #(1 1 1 1 1 1 1 1 1 1)) 0d0)
  (assert-approx= (standard-deviation #(1 2 3 2 1 2 3)) (sqrt 4/7)))

(defun standard-deviation (vector &key key)
  (let ((mean (average vector :key key)))
    (rms vector :key (lambda-key ((x key)) (- x mean)))))
#||

\subsection{Geometry}

Projecting a point onto a plane. The plane is defined by a point in
the plane and the normal vector of the plane.

||#
(deftest test-project-point ()
  (assert-equalp (project-point #(10d0 20d0 6)
                                :point-in-plane #(5d0 5d0 3d0) 
                                :normal-vector  #(0d0 0d0 1d0))
                 #(10d0 20d0 3d0))
  (assert-equalp (project-point #(10d0 20d0 6)
                                :point-in-plane  #(5d0 5d0 3d0) 
                                :normal-vector   #(0d0 1d0 0d0))
                 #(10d0 5d0 6d0)))

(defun project-point (point-to-project &key point-in-plane (normal-vector *nap-normal-vector*))
  (let* ((approximate-delta (map 'vector #'- point-to-project point-in-plane))
         (diff (inner-product approximate-delta (unit-vector normal-vector))))
    (map 'vector #'- point-to-project
         (map 'vector #'(lambda (x)
                          (* x diff))
              (unit-vector normal-vector)))))
#||



\subsection{Reading a file}

||#
(defun read-lines (stream)
  (loop :for line = (read-line stream nil nil) :while line :collect line))
#||



\subsection{CLOS}

The function \cl{class-slot-symbols} returns the names of the direct
slots of a class as a list of symbols. This type of introspection is
further on used to define standard \cl{print-object} methods using the
\cl{define-class-printer} macro.

||#
(defclass test-class-slot-symbols-dummy () (a b d cow))

(deftest test-class-slot-symbols ()
  (assert-equal (class-slot-symbols 'test-class-slot-symbols-dummy)
                '(a b d cow)))

(defun class-slot-symbols (class-name)
  (mapcar #'sb-mop:slot-definition-name
          (sb-mop:class-direct-slots (find-class class-name))))
#||


||#
(defmacro define-class-printer (class-name)
  `(defmethod print-object ((object ,class-name) stream)
     (let* ((class-slots (class-slot-symbols ',class-name))
            (length (reduce #'max class-slots 
                            :key #'(lambda (s)
                                     (length (symbol-name s))))))
       (format stream 
               (format nil "~~&<~~a>~~%~~{~~~aa: ~~a~~%~~}" length)
               ',class-name
               (zip class-slots
                    (mapcar #'(lambda (slot)
                                (funcall slot object))
                            class-slots))))))
#||

\subsection{Command line parsing}

The following macros and functions can be used to implement command
line parsing. A command line with the following structure is assumed:
{\footnotesize
\begin{verbatim}
user $ <binary> <command> <options-and-arguments>
\end{verbatim}}
for example
{\footnotesize
\begin{verbatim}
user $ statcor fit-plane --uncertainty 0.06 LOFAR-ECEF-01.csv -o solution.txt
\end{verbatim}}

One can define new commands, such as \cl{fit-plane} with the
\cl{defcommand} macro. The \cl{parse-command-line} macro can then be
used to go through the options and arguments after the command. A
command is implemented as a method on the generic function
\cl{execute-command}, which takes a symbol indicating the command
name, and a list of strings representing the remainder of the argument
list, excluding the command name itself in the above example it would
be
{\small\begin{lstlisting}
'("--uncertainty" "0.06" "LOFAR-ECEF-01.csv" "-o" "solution.txt")
\end{lstlisting}} 

||#
(defgeneric execute-command (command-name argv)
  (:documentation "Generic interface for command line commands."))

(defmacro defcommand (name (argv-var-name) &body body)
  `(defmethod execute-command ((command-name (eql ',name)) ,argv-var-name)
     ,@(or body `((error "The command \"~a\" is not yet implemented." ',name)))
     0))

(defun valid-command-p (symbol)
  (find-method #'execute-command '() 
               (list `(eql ,symbol) (find-class t))
               nil))

(defmacro parse-command-line (argv &body body)
  "Example usage:
   (parse-command-line argv
     ((\"-o\" \"--output\") (filename)
      (print (list 'output-filename filename)))
     ((\"-v\") () (print 'version-1)))"
  (let ((head-name (gensym))
        (argv-name (gensym))
        (arg-name  (gensym))
        (remaining-arguments-name (gensym)))
    `(let* ((,argv-name ,argv)
            (,head-name ,argv-name)
            (,remaining-arguments-name nil))
       (flet ((next-arg ()
                (unless ,head-name (error "Expected more arguments."))
                (pop ,head-name)))
         (loop :while ,head-name :do
            (let ((,arg-name (next-arg)))
              (cond
                ,@(mapcar #'(lambda (entry)
                              `((or ,@(mapcar #'(lambda (s) `(string= ,s ,arg-name)) (first entry)))
                                (let ,(mapcar #'(lambda (name) `(,name (next-arg))) (second entry))
                                  ,@(rest(rest entry)))))
                          body)
                (t (push ,arg-name ,remaining-arguments-name))))))
       (reverse ,remaining-arguments-name))))
#||

\subsection{Conditions}

||#
(defmacro define-format-condition (name superclasses slot-list (format-string &rest format-args))
  `(define-condition ,name ,superclasses
     ,(mapcar #'(lambda (slot-name)
                  `(,slot-name :initarg ,(intern (symbol-name slot-name) :keyword) :reader ,slot-name))
              slot-list)
     (:report (lambda (,name stream)
                (declare (ignore ,name))
                (format stream ,format-string ,@format-args)))))
