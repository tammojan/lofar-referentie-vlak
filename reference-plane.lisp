(in-package :lofar-reference-plane)

#||

\section{Introduction}

This document describes the relation between a local cartesian station
coordinate system with coordinates $p$, $q$, and $r$ and the Eurasian
cartesian ETRS89 system with geocentric coordinates $X$, $Y$, and
$Z$. A Common Lisp computer program that implements these relations is
described in the main text as well as in the appendices. The code in
the program is indicated by the line numbers in the margin. The
software has been implemented using the open source Steel Bank Common
Lisp (SBCL) system.



\section{Station coordinates}

\begin{figure}
\begin{center}
\includegraphics[width=0.6\textwidth]{station-pqr}
\end{center}
\caption{The local station $pqr$ coordinate system with a couple of
  LBA dipoles drawn as an example. The antennae have two dipoles,
  labelled ``X'' and ``Y'', as shown in the inset. The reference
  direction of the antenna is the bisector of the X and Y
  dipoles and is indicated with the dash-dotted line. The $r$ axis
  points upward out of the paper.}
\label{fig:station-pqr}
\end{figure}

The station coordinate system is illustrated in
Fig.~\ref{fig:station-pqr}. The $\unitvec{r}$ direction is the normal
vector to the ground plane of the
station. Section~\ref{sec:station-normal-vector}
describes how to derive this vector from a set of position
measurements of the station ground level in ETRS89 coordinates. For
all core stations and some of the remote stations near the core, this
vector is the normal vector to the NAP\footnote{Note that the normal
  to the GRS80 ellipsoid, given by
\begin{equation}
\vec{n}_\mathrm{grs80} = \left(\begin{array}{c}
\cos\phi\cos\lambda\\
\cos\phi\sin\lambda\\
\sin\phi
\end{array}\right) = 
\left(\begin{array}{c}
0.60123126\\
0.07246260\\
0.79578273
\end{array}\right)
\end{equation}
makes an angle of about 10.8$'$ with the NAP normal at the location of
the superterp (approximately $\lambda =0.1199451$~rad, $\phi =
0.9202990$~rad, and $h = 18.004$~m). This is not relevant to the problem of this
document, but is something we should not forget.} plane at the centre of the superterp:
\begin{equation}
\unitvec{r}_\mathrm{ref} = \columnvector{0.598753\\0.072099\\0.797682}.
\end{equation}
It was computed by Rienus Zwaneveld of Azimuth B.V. by converting
observed ETRS89 coordinates of wooden stakes at the perimeter of the
superterp to RD/NAP using the RDNAPTRANS transformation, setting the
NAP height of all markers artificially to 8.15~m, transforming back to
ETRS89 via RDNAPTRANS, and fitting a plane through the resulting
points.

||#
(defparameter *nap-normal-vector*      #(0.598753d0 0.072099d0 0.797682d0))
#||

As indicated in Fig.~\ref{fig:station-pqr}, a reference direction is
needed that indicates the northern bisector of the X and Y
dipoles, and therefore the $\unitvec{q}$ direction. In principle, this
is an arbitrary direction that lies in the ground plane of the
station. It is helpful if the reference directions of all LOFAR
stations are as similar as possible. A natural choice would be
the intersection of the ground plane of the station and a plane
parallel to the meridian plane at the centre of the superterp.

Let
\begin{equation}
\vec{m} = \columnvector{+Y_0\\-X_0\\0}\left(\sqrt{X_0^2+Y_0^2}\right)^{-1}
\end{equation}
be the normal vector of this meridian plane, where $X_0$ and $Y_0$ are
the earth-centred, earth-fixed (ECEF) ETRS89 $X$ and $Y$ coordinates
of the centre of the superterp. I use the average position of 76
wooden stakes around the superterp as the reference position.  These
measurements are listed in Tab.~\ref{tab:superterp-stakes}. I assume
that this average position is within 15~m of the centre of gravity of
the superterp, yielding a position accuracy of less than $1''$

||#
(deftest test-average-position ()
  (assert-approx= (average-position (mapcar #'make-position-measurement
                                            '((a 1 2 3 1)
                                              (b 4 5 6 1)
                                              (c 9 8 7 1))))
                  #(14/3 5 16/3)))

(defun average-position (position-measurements)
  (flet ((avg (fn)
           (average position-measurements :key fn)))
    (vector (avg #'etrs-x) (avg #'etrs-y) (avg #'etrs-z))))
#||

The result of calling this function is the vector (3826573.777
461045.133 5064895.754). After compensating approximately for an
antenna height of 1.705~m, I take
\begin{eqnarray}
X_0 & = & 3826574.0\\
Y_0 & = & 461045.0\\
Z_0 & = & 5064894.5,
\end{eqnarray}

||#
(defparameter *reference-position* 
  #(3826574.0d0 461045.0d0 5064894.5d0))
#||

which should be very close above the final surface of the
superterp. The normal vector of the reference direction plane is then
\begin{equation}
\vec{m} = \columnvector{0.119619948\\-0.992819756\\0}
\end{equation}

||#
(defparameter *reference-direction-plane-normal*
  (unit-vector (vector (aref *reference-position* 1)
                       (- (aref *reference-position* 0))
                       0d0)))
#||

The $q$ axis, and therefore the reference direction of the station, is given by
\begin{equation}
\unitvec{q} = \frac{\vec{m}\times\unitvec{r}}
{\|\vec{m}\times\unitvec{r}\|}.
\end{equation}

||#
(deftest test-reference-direction ()
  (assert-approx= (reference-direction #(0 0 1) #(1 0 0))
                  #(0 -1 0))
  (assert-approx= (reference-direction #(1 0 0) #(0 2 -3))
                  (unit-vector #(0 -3 -2)))
  (assert-approx= (reference-direction #(0 0 1))
                  (vector (aref *reference-direction-plane-normal* 1)
                          (- (aref *reference-direction-plane-normal* 0))
                          0d0)))

(defun reference-direction (normal-vector &optional 
                            (reference-direction-plane-normal
                             *reference-direction-plane-normal*))
  (unit-vector (cross-product (unit-vector reference-direction-plane-normal)
                              (unit-vector normal-vector))))
#||

Of course, in case of LOFAR stations in or close to the core,
\begin{equation}
\unitvec{r} = \unitvec{r}_\mathrm{ref}.
\end{equation}
In case of other stations, $\unitvec{r}$ should be determined from a
cloud of ETRS89 coordinates of locations on the flattened surface of
the station using the procedure outlined in
Sect.~\ref{sec:station-normal-vector}.

The resulting reference direction at the superterp is
\begin{equation}
\unitvec{q}_\mathrm{core} =
\columnvector{-0.791954\\-0.095419\\0.603078}/\left(1-5\cdot10^{-7}\right)
\end{equation}

||#
(defparameter *reference-direction-superterp*
  (unit-vector #(-0.791954d0 -0.095419d0 0.603078d0)))
#||

The $p$ direction of a station coordinate system is given by the cross
product of the $\unitvec{q}$ and $\unitvec{r}$ vectors, which by now
are known.
\begin{equation}
\unitvec{p} = \unitvec{q}\times\unitvec{r}
\end{equation}

We can now fully specify the transformation from local station
coordinates to ETRS89 and vice versa. Given a normal vector
$\unitvec{r}$ and an ETRS89 position $\vec{X}_0$ of a point on the
ground plane of the station that will have known (or defined) station
coordinates
\begin{equation}
  \vec{p}_0 = \columnvector{p_0\\q_0\\r_0},
\end{equation}
the transformation from station coordinates $\vec{p}$ to ETRS89
coordinates $\vec{X}$ is
\begin{equation}
\vec{X} = \vec{X}_0 + \matrix{T}\left(\vec{p} -\vec{p}_0\right),
\end{equation}
where $\matrix{T}$ is a rotation matrix with columns $\unitvec{p}$,
$\unitvec{q}$, and $\unitvec{r}$:
\begin{equation}
\matrix{T} = \left(\begin{array}{lll}
    \hat{p}_x & \hat{q}_x & \hat{r}_x\\
    \hat{p}_y & \hat{q}_y & \hat{r}_y\\
    \hat{p}_z & \hat{q}_z & \hat{r}_z\\
\end{array}
\right)
\end{equation}

||#
(deftest test-station-to-etrs89-matrix ()
  (assert-approx= (station-to-etrs89-matrix #(1 0 0))
                  #2A((0 0 1)
                      (1 0 0)
                      (0 1 0)))
  (assert-approx= (station-to-etrs89-matrix #(0 0 1))
                  #2A((-0.1196199483963d0  -0.9928197560210d0  0)
                      ( 0.9928197560210d0  -0.1196199483963d0 0)
                      ( 0             0            1)))
  (assert-approx= (station-to-etrs89-matrix #(0 -1 0))
                  #2A((-1   0  0)
                      ( 0   0 -1)
                      ( 0  -1  0))))

(defun station-to-etrs89-matrix (normal-vector)
  (let* ((r-unit (unit-vector normal-vector))
         (q-unit (reference-direction normal-vector))
         (p-unit (unit-vector (cross-product q-unit r-unit))))
    (make-array '(3 3)
                :initial-contents
                (map 'list #'list p-unit q-unit r-unit))))
#||

The following function transforms station $p$, $q$, $r$ coordinates to
ETRS89 $X$, $Y$, $Z$.

||#
(deftest test-transform-station-to-etrs89 ()
  (assert-error (transform-station-to-etrs89 #(0 0 0)) error)
  (assert-approx= (transform-station-to-etrs89 
                   #(1 -2 0)
                   :normal-vector #(0  0 1)
                   :station-0     #(1 -2 0)
                   :etrs89-0      #(3827000d0 461000d0 5065000d0))
                  #(3827000d0 461000d0 5065000d0))
  (assert-approx= (transform-station-to-etrs89 
                   #(1 -2 30)
                   :normal-vector #(0 0 1)
                   :station-0     #(0 0 0)
                   :etrs89-0      #(3827000d0 461000d0 5065000d0))
                  #(3827001.8660195637d0 461001.2320596528d0 5065030.0d0)))
                                                                
(defun transform-station-to-etrs89 (station-point &key
                                    normal-vector
                                    (station-0 #(0d0 0d0 0d0))
                                    (etrs89-0  #(0d0 0d0 0d0)))
  (unless normal-vector
    (error "Please provide a normal vector for the station"))
  (map 'vector #'+ etrs89-0 (matrix-vector-product
                             (station-to-etrs89-matrix normal-vector)
                             (map 'vector #'- station-point station-0))))
#||

the inverse transformation is
\begin{equation}
\vec{p} = \vec{p}_0 + \matrix{T}^{-1} \left(\vec{X} - \vec{X}_0\right).
\end{equation}

||#
(deftest test-transform-etrs89-to-station ()
  (assert-error (transform-etrs89-to-station #(0 0 0)) error)
  (assert-approx= (transform-etrs89-to-station
                   #(3827000d0 461000d0 5065000d0)
                   :normal-vector #(0 0 1)
                   :station-0     #(1 -2 0)
                   :etrs89-0      #(3827000d0 461000d0 5065000d0))
                  #(1 -2 0))
  (assert-approx= (transform-etrs89-to-station
                   #(3827001.8660195637d0 461001.2320596528d0 5065030.0d0)
                   :normal-vector #(0 0 1)
                   :station-0     #(0 0 0)
                   :etrs89-0      #(3827000d0 461000d0 5065000d0))
                  #(1 -2 30)))

(defun transform-etrs89-to-station (etrs89-point &key
                                    normal-vector
                                    (station-0 #(0d0 0d0 0d0))
                                    (etrs89-0  #(0d0 0d0 0d0)))
  (unless normal-vector
    (error "Please provide a normal vector for the station"))
  (map 'vector #'+ station-0
       (matrix-vector-product
        (inverse (station-to-etrs89-matrix normal-vector))
        (map 'vector #'- etrs89-point etrs89-0))))
#||






\section{Determining the normal vector of a station}
\label{sec:station-normal-vector}



\subsection{Data model}

The equation of a plane is
\begin{equation}
z = ax + by + c,
\end{equation}
which is linear in the coefficients $a$, $b$, and $c$.  The problem
can therefore be cast in the form
\begin{equation}
\matrix{M}\left(\begin{array}{c}
a\\
b\\
c
\end{array}\right) = \vec{z},
\end{equation}
where row $i$ of the model matrix $\matrix{M}$ is equal to
\begin{equation}
\left(x_i\ \  y_i\ \ 1\right)
\end{equation}
and the vector $\vec{z}$ contains the $z$ coordinates.

||#
(deftest test-model-matrix ()
  (assert-equalp (model-matrix (list (make-position-measurement '(11 1  2  3) :uncertainty 0.06d0)
                                     (make-position-measurement '(22 4  5  6 0.06d0))
                                     (make-position-measurement '(33 7  8  9) :uncertainty 0.06d0)))
                 #2A((1 2 1d0)
                     (4 5 1d0)
                     (7 8 1d0))))

(defun model-matrix (position-measurements)
  (make-array (list (length position-measurements) 3)
              :initial-contents
              (mapcar #'(lambda (mp)
                          (list (etrs-x mp)
                                (etrs-y mp)
                                1d0))
                      position-measurements)))
#||

Now we have the model matrix, we can solve for the parameters of the
plane using the \cl{cl-linalg} package. The solution of the
\cl{linear-least-squares} function must be converted to a normal
vector of the plane with associated errors. These values are stored in
a \cl{normal-plane-solution} object.

||#
(defclass normal-plane-solution ()
  ((normal-vector :initarg :normal-vector :reader normal-vector)
   (uncertainties :initarg :uncertainties :reader uncertainties)))

(defmethod print-object ((object normal-plane-solution) stream)
  (let ((v     (to-list (normal-vector object)))
        (sigma (to-list (uncertainties object))))
    (format stream "<NORMAL-PLANE-SOLUTION>~%~{~a: ~8,6f +/- ~8,6f~^~%~}
Direction uncertainty (max): ~5,2f\"
Direction uncertainty (rms): ~5,2f\"
Direction uncertainty (avg): ~5,2f\"~%"  
            (zip '(X Y Z) v sigma)
            (arcsec-from-rad (reduce #'max (uncertainties object)))
            (arcsec-from-rad (rms (uncertainties object)))
            (arcsec-from-rad (average (uncertainties object)))
            )))
#||

The normal vector
\begin{equation}
\vec{n}_\mathrm{obs} =
\left(\begin{array}{c}
-a\\
-b\\
1
\end{array}\right)\left(\sqrt{a^2 + b^2 + 1}\right)^{-1},
\end{equation}
and the uncertainties in the elements of this vector are computed using
standard error propagation:
\begin{eqnarray}
  l            & = & \sqrt{a^2 + b^2 + 1}\\
  \sigma^2_{n_x} & = & 
  \left(\frac{a^2 - l^2}{l^3}\right)^2\sigma^2_a +
  \left(\frac{ab}{l^3}\right)^2\sigma^2_b \label{eqn:sigma-normal-x}\\
  \sigma^2_{n_y} & = & 
  \left(\frac{ab}{l^3}\right)^2\sigma^2_a +
  \left(\frac{b^2 - l^2}{l^3}\right)^2\sigma^2_b\label{eqn:sigma-normal-y}\\
  \sigma^2_{n_z} & = & \left(\frac{-a}{l^3}\right)^2\sigma^2_a +
  \left(\frac{-b}{l^3}\right)^2\sigma^2_b.\label{eqn:sigma-normal-z}
\end{eqnarray}

The functions \cl{sigma-normal-*} implement
Eqs. (\ref{eqn:sigma-normal-x})~--~(\ref{eqn:sigma-normal-z}). 

||#
(deftest test-sigma-normal-x ()
  (assert-approx= (sigma-normal-x 1d0 2d0 3d0 4d0)
                  (sqrt (+ (/ 225d0 216d0) (/ 64d0 216d0)))))

(defun sigma-normal-x (a b sigma-a sigma-b)
  (let ((l (sqrt (+ (square a) (square b) 1))))
    (sqrt (+ (square (* (/ (- (square a) (square l))
                           (cube l))
                        sigma-a))
             (square (* (/ (* a b)
                           (cube l))
                        sigma-b))))))


(deftest test-sigma-normal-y ()
  (assert-approx= (sigma-normal-y 1d0 2d0 3d0 4d0)
                  (sqrt (+ (/ 36d0 216d0) (/ 64d0 216d0)))))

(defun sigma-normal-y (a b sigma-a sigma-b)
  (let ((l (sqrt (+ (square a) (square b) 1))))
    (sqrt (+ (square (* (/ (* a b)
                           (cube l))
                        sigma-a))
             (square (* (/ (- (square b) (square l))
                           (cube l))
                        sigma-b))))))


(deftest test-sigma-normal-z ()
  (assert-approx= (sigma-normal-z 1d0 2d0 3d0 4d0)
                  (sqrt (+ (/ 9d0 216d0) (/ 64d0 216d0)))))

(defun sigma-normal-z (a b sigma-a sigma-b)
  (let ((l (sqrt (+ (square a) (square b) 1))))
    (sqrt (+ (square (* (/ a (cube l)) sigma-a))
             (square (* (/ b (cube l)) sigma-b))))))
#||

The function \cl{make-normal-plane-solution} collects the normal vector and
associated uncertainties in a \cl{normal-plane-solution} object.

||#
(deftest test-make-normal-plane-solution ()
  (assert-equal (type-of (make-normal-plane-solution 1d0 2d0 3d0 4d0))
                'normal-plane-solution)
  (assert-approx=
   (normal-vector (make-normal-plane-solution 1d0 2d0 3d0 4d0))
   (unit-vector #(-1d0 -2d0 1d0))))

(defun make-normal-plane-solution (a b sigma-a sigma-b)
  (make-instance 
   'normal-plane-solution
   :normal-vector (unit-vector (vector (- a) (- b) 1))
   :uncertainties (map 'vector
                       #'(lambda (fn)
                           (funcall fn a b sigma-a sigma-b))
                       '(sigma-normal-x sigma-normal-y sigma-normal-z))))
#||

\subsection{Solution scheme}
\label{sec:solution-scheme}

For convenience, we define some readers on the result of
\cl{linear-least-squares}.

||#
(defmethod coef-a  ((result solver-solution-info)) (aref (solution result) 0))
(defmethod coef-b  ((result solver-solution-info)) (aref (solution result) 1))
(defmethod coef-c  ((result solver-solution-info)) (aref (solution result) 2))
(defmethod sigma-a ((result solver-solution-info)) (aref (uncertainties result) 0))
(defmethod sigma-b ((result solver-solution-info)) (aref (uncertainties result) 1))
(defmethod sigma-c ((result solver-solution-info)) (aref (uncertainties result) 2))
#||

For quality assessment, we want to compute the vector of residuals
given a model solution and a list of \cl{position-measurement}s. These
residuals are subsequently used to A) determine the standard deviation
of the points used for the fit, and B) determine which of ALL points
are inside the 3$\sigma$ boundary and will be selected for the next
fit.

||#
(deftest test-residuals ()
  (assert-approx= (residuals (mapcar #'make-position-measurement
                                     '((a -1  5 2 1)
                                       (b  0 -2 4 1)
                                       (c  5  1 3 1)))
                             #(1d0 2d0 3d0))
                  #(-10d0 5d0 -7d0)))

(defun residuals (position-measurements solution-vector)
  (map 'vector #'-
       (map 'vector #'etrs-z position-measurements)
       (evaluate-model (model-matrix position-measurements)
                       solution-vector)))
#||

The implementation of \cl{fit-plane} is now relatively
straightforward. The argument \cl{position-measurements} contains the list
of all points that may or may not be included in the fit. The argument
\cl{selected-position-measurements}, if provided, contains a subset of that
list, which will be used in the subsequent solution. If it is not
provided, all \cl{position-measurements} are used. After the fit, the
standard deviation of the points included in the fit is computed, as
well as the residuals of all points in \cl{position-measurements}. All
points in \cl{position-measurements} with residuals smaller than $3\sigma$
will be used in a subsequent solution. This procedure takes care of
automatically discarding outliers.

||#
(deftest test-discard-outliers ()
  (let ((mps (mapcar #'make-position-measurement
                     '((a -1  5 2 1)
                       (b  0 -2 4 1)
                       (c  5  1 3 1)))))
    (assert-equal (mapcar #'position-id
                          (discard-outliers mps
                                            #(1d0 2d0 3d0)
                                            (/ 7.1d0 3d0)))
                  '(b c))
    (assert-equal (mapcar #'position-id
                          (discard-outliers mps
                                            #(1d0 2d0 3d0)
                                            (/ -7.1d0 3d0)))
                  '(b c))
    (assert-equal (mapcar #'position-id
                          (discard-outliers mps
                                            #(1d0 2d0 3d0)
                                            (/ -6.9d0 3d0)))
                  '(b))))

(defun discard-outliers (position-measurements parameter-vector sigma &key verbose)
  (when verbose 
    (format t "~&~a = ~a~%" #\GREEK_SMALL_LETTER_SIGMA sigma))
  (let ((abs-sigma (abs sigma)))
    (loop 
       :for mp :in     position-measurements
       :for dz :across (residuals position-measurements parameter-vector)
       :if (< (abs dz) (* 3 abs-sigma))
       :collect mp
       :else :when verbose :do (format t "~&Discarded: ~a~%" (position-id mp)))))
#||

If the new selection is equal to
the \cl{selected-position-measurements}, there is no need to recompute a
fit. We have converged and return the solution info and associated
reference plane.

||#
(deftest test-fit-plane ()
  (let* ((best-fit (fit-plane 
                    (parse-file *raw-position-file-name-csv*
                                :uncertainty 0.06d0)
                    :verbose t))
         (dm (getf best-fit :discarded))
         (si (getf best-fit :solution-info))
         (rp (getf best-fit :normal-plane-solution)))
    (assert-equal (mapcar #'position-id dm)
                  '(10071 10070 10069))
    (assert-approx= (normal-vector rp)
                    #(0.598661d0 0.072071d0 0.797753d0)
                    :epsilon 1d-6 :absolute t)
    (assert-approx= (uncertainties rp)
                    #(0.000043d0 0.000055d0 0.000032d0)
                    :epsilon 1d-6 :absolute t)
    (assert-approx= (reduced-chi-squared  si) 1.5106092962486666d0)))

(defun fit-plane (position-measurements &key selected-position-measurements verbose)
  (let* ((markers            (mapcar #'validate-position-measurement
                                     (or selected-position-measurements
                                         position-measurements)))
         (solution-info      (linear-least-squares
                              (model-matrix         markers)
                              (mapcar #'etrs-z      markers)
                              (mapcar #'uncertainty markers)))
         (new-selection (discard-outliers
                         position-measurements
                         (solution solution-info)
                         (standard-deviation
                          (residuals markers
                                     (solution solution-info)))
                         :verbose verbose)))

      (if (equal new-selection selected-position-measurements)
          (list :used
                new-selection

                :discarded
                (set-difference position-measurements new-selection)

                :solution-info
                solution-info 

                :normal-plane-solution 
                (make-normal-plane-solution
                 (coef-a  solution-info) (coef-b  solution-info)
                 (sigma-a solution-info) (sigma-b solution-info)))

          (fit-plane position-measurements
                     :selected-position-measurements new-selection
                     :verbose                   verbose))))
#||






\section{Example: the superterp}

\subsection{Data}

It was decided to use the NAP plane at the location of the superterp
as the reference plane of LOFAR because many of the position markers
have been disturbed by ground work, making their positions rather
inaccurate. It is nevertheless interesting to investigate how close
the plane through the position markers still is to the NAP plane.

The observations were done on Monday September 1$^\mathrm{st}$ 2008 by
Rienus Zwaneveld (Azimuth), Hans K\"onig (Azimuth), and Michiel
Brentjens (ASTRON). The measurements were performed between 8:55 and
10:05 local time. We used differential GPS with a virtual base station
that was contacted through a mobile telephone. The phase centre of the
GPS antenna was 1.705 ~m above the tip of the staff of the receiver.

The staff was placed on top of a position marker, aligning the staff
to the local vertical direction using a level indicator. Each
measurement consisted of 20 individual observations with standard
deviations of approximately 8, 8 and 13~cm in RD-X, RD-Y, and
RD-H. The average therefore has accuracies of the order of 2, 2, and
3~cm, which translate to accuracies of the order of 3~cm in ETRS-X,
ETRS-Y, and ETRS-Z. Table~\ref{tab:superterp-stakes} lists the
observations.


\begin{table}
\caption{Position marker observations.}
\label{tab:superterp-stakes}
\begin{center}
\tiny
\begin{tabular}{llllllll}
\hline
\hline
ID & ETRS-X & ETRS-Y & ETRS-Z & RD-X & RD-Y & RD-Z & Error\\
   & (m)    & (m)    & (m)    & (m)  & (m)  & (m)  & (m)\\ 
\hline
10000&    3826597.2075653&    460842.23827549&    5064896.5670354&        254543.50282961&    548583.28123074&    8.1686942280269&    0.06\\
10001&    3826609.0108658&    460843.95827016&    5064887.4821479&        254544.10649704&    548568.29977077&    8.1112111866923&    0.06\\
10002&    3826642.9608768&    460856.32348526&    5064860.9124205&        254553.22600674&    548524.3874796&     8.130896246036&     0.06\\
10003&    3826663.5405048&    460870.33948328&    5064844.2343701&        254565.24624494&    548496.93581063&    8.1566753314786&    0.06\\
10004&    3826665.7594512&    460871.56419892&    5064842.439327&         254566.25728819&    548493.99924713&    8.1413776057242&    0.06\\
10005&    3826682.6301798&    460888.93833183&    5064828.225256&         254581.97014755&    548470.72629801&    8.154699136424&     0.06\\
10006&    3826689.9034521&    460898.99945718&    5064821.8473183&        254591.30422004&    548460.34936711&    8.1464164004255&    0.06\\
10007&    3826697.2225018&    460910.24290331&    5064815.2872654&        254601.81172565&    548449.73759982&    8.1055420134513&    0.06\\
10008&    3826703.8916306&    460922.59964504&    5064809.1892155&        254613.48791704&    548439.83709128&    8.124456558087&     0.06\\
10009&    3826709.5842924&    460935.14925191&    5064803.7754716&        254625.44834026&    548431.11027756&    8.1186377711139&    0.06\\
10010&    3826714.9751922&    460950.65407156&    5064798.4155534&        254640.37845943&    548422.43394323&    8.1882484684427&    0.06\\
10011&    3826721.1848626&    460976.74378505&    5064791.3854402&        254665.77214996&    548411.30639826&    8.1790459021983&    0.06\\
10012&    3826723.0916981&    460991.4355077&     5064788.6043148&        254680.22161612&    548407.01314268&    8.1615437534064&    0.06\\
10013&    3826723.0964947&    460991.44028622&    5064788.6023066&        254680.22589801&    548407.00776355&    8.1631579078457&    0.06\\
10014&    3826723.9907318&    461006.20917862&    5064786.5785013&        254694.84735766&    548403.96959874&    8.1491908194664&    0.06\\
10015&    3826723.9559623&    461021.18002005&    5064785.3205367&        254709.756171&      548402.11590557&    8.2045989795644&    0.06\\
10016&    3826722.8496678&    461036.11198554&    5064784.7471935&        254724.7285504&     548401.5289845&     8.1618997951258&    0.06\\
10017&    3826720.8150644&    461049.48176853&    5064785.0413195&        254738.2323898&     548402.31957205&    8.142822421468&     0.06\\
10018&    3826720.8093555&    461049.47425433&    5064785.0348377&        254738.22558653&    548402.32076243&    8.1336918663041&    0.06\\
10019&    3826717.8370083&    461065.54978379&    5064785.8032271&        254754.51151093&    548403.93917479&    8.1267549517346&    0.06\\
10020&    3826713.8884391&    461079.98807233&    5064787.2634142&        254769.26137178&    548406.87287446&    7.969201569712&     0.06\\
10021&    3826708.9932885&    461093.83423017&    5064789.7279238&        254783.50767425&    548411.20834294&    8.0034724319419&    0.06\\
10022&    3826689.3245039&    461132.51074932&    5064801.1633327&        254823.86496937&    548430.82444186&    8.1410637448628&    0.06\\
10023&    3826681.0858118&    461144.12677611&    5064806.3156328&        254836.20540388&    548439.60337393&    8.1570963036714&    0.06\\
10024&    3826672.2060238&    461154.78947144&    5064812.0149659&        254847.65714119&    548449.29282982&    8.1569794407235&    0.06\\
10025&    3826662.564524&     461164.45487807&    5064818.3502321&        254858.18782369&    548460.04550816&    8.1362529658367&    0.06\\
10026&    3826652.5829493&    461173.37922124&    5064824.8497332&        254868.01456814&    548471.22290782&    7.989524255007&     0.06\\
10027&    3826642.1047877&    461181.19274539&    5064832.2116367&        254876.77713692&    548483.39798695&    8.153380079941&     0.06\\
10028&    3826631.1312725&    461187.97073991&    5064839.8572828&        254884.55746219&    548496.21526093&    8.1723660903427&    0.06\\
10029&    3826619.5666297&    461193.39807666&    5064847.8064195&        254891.05178428&    548509.78555532&    7.9821639058103&    0.06\\
10030&    3826609.2764953&    461197.00601864&    5064855.3509078&        254895.6098933&     548522.23680971&    8.1009004992009&    0.06\\
10031&    3826605.5890006&    461198.42043464&    5064858.0139648&        254897.36464764&    548526.66520027&    8.1198913458386&    0.06\\
10032&    3826602.7799935&    461199.3048017&     5064860.0538852&        254898.50918028&    548530.05988866&    8.1294391356765&    0.06\\
10033&    3826596.30722&      461201.10372815&    5064864.7601544&        254900.90882994&    548537.90359782&    8.1387534663886&    0.06\\
10034&    3826584.2578844&    461203.00160287&    5064873.6376407&        254903.93140166&    548552.68409296&    8.1444284575947&    0.06\\
10035&    3826572.2188236&    461203.84898954&    5064882.6843339&        254905.90602353&    548567.63723053&    8.2154794533711&    0.06\\
10036&    3826560.2487693&    461203.46400223&    5064891.6902876&        254906.64833276&    548582.60343357&    8.206455867984&     0.06\\
10037&    3826560.2430332&    461203.47184273&    5064891.6878396&        254906.65675385&    548582.60592589&    8.2016347631341&    0.06\\
10038&    3826548.2888066&    461201.60360828&    5064900.8403484&        254905.92040953&    548597.75916497&    8.2120160456699&    0.06\\
10039&    3826548.2821331&    461201.60608642&    5064900.8439623&        254905.923519&      548597.76645863&    8.2110827599172&    0.06\\
10040&    3826536.588204&     461198.81261051&    5064909.8674002&        254904.24165636&    548612.70452879&    8.2075981990679&    0.06\\
10041&    3826536.5684134&    461198.8116359&     5064909.8632981&        254904.2427831&     548612.717849&      8.1924078730978&    0.06\\
10042&    3826525.069528&     461194.53736309&    5064918.804035&         254901.06902888&    548627.56220846&    8.1329006723291&    0.06\\
10043&    3826514.1774686&    461189.06102193&    5064927.5463086&        254896.63948272&    548641.89507482&    8.191640377839&     0.06\\
10044&    3826503.4800975&    461182.35056096&    5064936.0253501&        254890.96566051&    548656.00717649&    8.067922903179&     0.06\\
10045&    3826503.4949492&    461182.35646961&    5064936.0157391&        254890.97012162&    548655.98914225&    8.0695725446813&    0.06\\
10046&    3826493.4644997&    461174.99743167&    5064944.2764953&        254884.58503164&    548669.48854829&    8.1241980685093&    0.06\\
10047&    3826483.8598945&    461166.09335751&    5064952.3007514&        254876.62221713&    548682.62301367&    8.1336393488941&    0.06\\
10048&    3826467.1372328&    461146.49651123&    5064966.7097845&        254858.68098433&    548706.06262829&    8.2041810427714&    0.06\\
10049&    3826459.7451379&    461135.14223287&    5064973.274837&         254848.07073026&    548716.74375084&    8.1973251157498&    0.06\\
10050&    3826453.2647975&    461123.42540281&    5064979.0965221&        254837.01489636&    548726.27970173&    8.1171131107656&    0.06\\
10051&    3826447.661552&     461110.08130119&    5064984.5594841&        254824.25443513&    548735.02475715&    8.1584173082387&    0.06\\
10052&    3826447.669468&     461110.07229812&    5064984.562586&         254824.24462446&    548735.02101447&    8.1649811590163&    0.06\\
10053&    3826443.1100086&    461097.33725157&    5064989.1100213&        254811.99341103&    548742.33935729&    8.1447121701068&    0.06\\
10054&    3826438.4674419&    461082.72362725&    5064993.9336086&        254797.87897421&    548750.03110089&    8.1595210901703&    0.06\\
10055&    3826435.1087681&    461059.33833802&    5064998.5443977&        254774.91043051&    548757.23268218&    8.1404602782975&    0.06\\
10056&    3826434.3134694&    461054.00906974&    5064999.6690049&        254769.67830745&    548758.94211528&    8.1771557003651&    0.06\\
10057&    3826433.4108814&    461039.52485683&    5065001.6551805&        254755.34130744&    548761.9428658&     8.1766346283606&    0.06\\
10058&    3826433.4173085&    461039.53108287&    5065001.6451817&        254755.34695952&    548761.93126588&    8.1729544098096&    0.06\\
10059&    3826433.3507495&    461024.18457883&    5065003.0501216&        254740.07316543&    548763.9822205&     8.1470451142608&    0.06\\
10060&    3826434.4721282&    461009.24637745&    5065003.5982773&        254725.09333296&    548764.54242909&    8.1782276073781&    0.06\\
10061&    3826439.5678703&    460979.69594398&    5065002.4225403&        254695.19123948&    548762.00250083&    8.1594890081818&    0.06\\
10062&    3826443.4128687&    460965.28658507&    5065000.71062&          254680.48396858&    548758.99712323&    8.0563095855415&    0.06\\
10063&    3826454.1223326&    460937.76074406&    5064995.2107178&        254652.06903387&    548749.23940144&    8.0945941364707&    0.06\\
10064&    3826460.6742005&    460924.96944905&    5064991.4047708&        254638.7173129&     548742.70014215&    8.0580466128991&    0.06\\
10065&    3826460.6724456&    460924.98037437&    5064991.4081699&        254638.72831821&    548742.70276573&    8.0604956154576&    0.06\\
10066&    3826476.2775555&    460901.24983521&    5064981.8584453&        254613.63244242&    548726.33020779&    8.0724997983415&    0.06\\
10067&    3826485.282328&     460890.61404637&    5064976.0346904&        254602.19609472&    548716.46443498&    8.0501150680099&    0.06\\
10068&    3826494.6321484&    460880.70950142&    5064969.8019091&        254591.45643947&    548706.02315294&    7.9607390384826&    0.06\\
10069&    3826504.5099688&    460872.03539754&    5064963.0981795&        254581.89183217&    548694.78626427&    7.900486118837&     0.06\\
10070&    3826504.5181393&    460872.03363293&    5064963.0924733&        254581.88930363&    548694.77646656&    7.9006977471915&    0.06\\
10071&    3826515.1204029&    460863.96359458&    5064955.8078035&        254572.8578316&     548682.56860677&    7.8542550436102&    0.06\\
10072&    3826526.58982&      460857.31790446&    5064948.2110779&        254565.1572613&     548669.37698576&    8.1807068622406&    0.06\\
10073&    3826537.8206859&    460851.9831495&     5064940.2999778&        254558.78905085&    548656.08700227&    8.2081077043945&    0.06\\
10074&    3826549.4480219&    460847.74197273&    5064931.8956137&        254553.47363161&    548642.10270291&    8.1582223335594&    0.06\\
10075&    3826561.2862677&    460844.54398564&    5064923.3003558&        254549.17631828&    548627.75754563&    8.1575550887838&    0.06\\
\hline
\end{tabular}
\normalsize
\end{center}
\end{table}

The contractor estimated that because of ground work, the position
accuracies of the position markers are of the order of
5~cm. Furthermore, there were some markers that were placed at a lower
height of 60~cm above the field, while most markers were initially
placed at 80~cm above the field. The scatter is therefore expected to
be somewhat larger than 5~cm. The actual scatter combined with the
uncertainty in the position markers, leads to a total expected
accuracy with respect to the average plane of about 6~cm per marker.

The data are stored in an ASCII file with 8 columns: The number, ETRS
X, Y, and Z, RD X, Y, and H, and an error estimate of the absolute
position combined with the GPS uncertainty. This ASCII file may either
be a plain text file or a semicolon-separated CSV file. Reading the
file is explained in Appendix~\ref{sec:reading-the-data}.








\subsection{Analysis}

The ETRS89 data are stored in a CSV file with fields separated by
semicolons.

||#
(defparameter *raw-position-file-name-csv*
  (merge-pathnames #P"git-projects/lofar-referentie-vlak/data/LOFAR-ECEF-01.csv"
                   (user-homedir-pathname)))
#||

Points 10069, 10070, and 10071 are excluded from the final fit because
their residuals were more than $3\sigma$, where $\sigma$ is the
standard deviation of the residuals from the points that were used to
derive the plane.

The following plane solution is obtained:
\begin{eqnarray}
  a & = & -0.750434 \pm 0.000084\\
  b & = & -0.090343 \pm 0.000070\\
  c & = &  7978139 \pm 326\\
  \unitvec{r} & = & \columnvector{0.598661\\0.072071\\0.797753}
  \pm
  \columnvector{0.000043\\0.000055\\0.000032}\\
\sigma & = & 7\ \mbox{cm}\\
\chi^2_\mathrm{red} & = & 1.5
\end{eqnarray}

||#
(defparameter *observed-normal-vector* #(0.598661d0 0.072071d0 0.797753d0))
#||

The uncertainty in vector $\unitvec{r}$ corresponds to an angular
uncertainty of approximately $10''$, or $0.17'$.
The angle between this vector and $\unitvec{r}_\mathrm{ref}$ is $24''$, or $0.4'$. This deviation
is at the 2 to 3$\sigma$ level. It corresponds to a difference of
12~mm over a distance of 100~m, which is well within the limits set
for the HBA field (3~cm over 100~m, or 1'). Although the stakes have
clearly been affected by all the ground work at the superterp, the
average plane still has the same orientation. The corresponding
rotation matrix
\begin{equation}
\matrix{T} = \left(\begin{array}{lll}
    -0.11958526709404055    & -0.7920252183723586  & 0.598661354487046 \\
    0.9928239329096564      & -0.09542720637428782 & 0.07207139880286229 \\
    0.000046215247707676    &  0.6029839979185325  & 0.7977532802303818 \\
  \end{array}\right).
\end{equation}

||#

