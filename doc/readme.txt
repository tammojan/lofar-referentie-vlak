%                                                             readme.txt
% AA vers. 7.0, LaTeX class for Astronomy & Astrophysics
% read-me file
%                                                 (c) Springer-Verlag HD
%                                                revised by EDP Sciences
%                                            tex-support@edpsciences.org
%-----------------------------------------------------------------------
%
%-----------------------------------------------------------------------
% What's New in AA v7.0 (January 2010)
%-----------------------------------------------------------------------

- The 2010 update is now compatible with the new version of the Natbib 
package, version 2009/02/02 8.3.

- The authors' institutes can also be given using labels, so that there is 
no need to rewrite the full institutes list if the order of the authors 
changes during the evaluation process.

- Notes to tables: This new version includes new commands to format the 
table notes in the proper A\& A layout. 

The following files are part of the macro package AA 

  readme.txt      This file
  aa.cls          The document class file
  aadoc.pdf       User's Guide 
  aa.dem          Example of an article (LaTeX source)


  bibtex/       Directory for BIBTeX style
   aa.bst       Bibliography style file 
   natbib.sty   This package reimplements the LaTeX \cite command 
   natnotes.pdf Brief reference sheet for Natbib        

Remember to transfer dvi and pdf files as binaries!
%
%-----------------------------------------------------------------------
%
% History
%-----------------------------------------------------------------------
This directory contains the LaTeX2e support for the new 2001
Astronomy and Astrophysics journal.

In order to ensure the smoothest transition for both authors and publishers,
Springer-Verlag has kindly granted EDP Sciences the permission to use the
LaTeX macro package that they developed for A&A Main journal.

Only  minor changes have been incorporated between the Springer class (1999)
and the EDP Sciences class for the new journal.
Please, note:
- that the abstract is now a command (\abstract{...}) and not an environment,  
so the \maketitle command has to be placed after the abstract and the keywords
- and that the command \thesaurus does no longer exist.

The journal is now printing using the Postscript TX Times-fonts. 
The TX fonts consist of virtual text roman fonts using Adobe Times with some 
modified and additional text symbols. 
The TX fonts are distributed under the GNU public license and are available 
in the LaTeX distributions since December 2000.
In any case, these fonts and the installation guide are also available in 
every public TeX archive server, i.e.:

http://www.tug.org/tex-archive/fonts/txfonts/
ftp://ftp.dante.de/tex-archive/fonts/txfonts/
ftp://ftp.tex.ac.uk/tex-archive/fonts/txfonts/

Please contact your system administrator to install it. 

Since this change of fonts results in a slightly different page make-up from 
CM fonts, we encourage you to use TX fonts.
To proceed, all you need is: 
\documentclass{aa}
\usepackage{txfonts}
