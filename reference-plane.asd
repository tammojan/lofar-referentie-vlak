(defpackage :reference-plane-system
  (:use :common-lisp :asdf))

(in-package :reference-plane-system)



(defsystem :reference-plane
  :description   "reference-plane"
  :license       "Copyright 2008 Michiel Brentjens"
  :author        "Michiel Brentjens <brentjens@astron.nl>"
  :version       "0.1"
  :depends-on    ("unit-test" "cl-linalg" "astrolisp" "cl-utilities" "cl-pdf")
  :components  
  ((:file "package")
   (:file "utilities"       :depends-on ("package"))
   (:file "help-text"       :depends-on ("package"))
   (:file "csv-txt-input"   :depends-on ("package" "utilities"))
   (:file "map-making"      :depends-on ("package"))
   (:file "reference-plane" :depends-on ("package" "utilities" "csv-txt-input"))
   (:file "main" :depends-on ("package" "reference-plane" "utilities" "csv-txt-input" "help-text"))
   (:file "unit-test-execution" :depends-on ("package" "utilities" "reference-plane" "main" "csv-txt-input" "help-text" "map-making"))
   ))