from numpy import arctan2, sin, cos, sqrt, pi

fmt='''<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Placemark>
    <name>Simple placemark</name>
    <description>Attached to the ground. Intelligently places itself 
       at the height of the underlying terrain.</description>
    <Point>
      <coordinates>-122.0822035425683,37.42228990140251,0</coordinates>
    </Point>
  </Placemark>
</kml>'''



def normalized_earth_radius(lat_rad):
    wgs84_f = 1./298.257223563
    return 1./sqrt(cos(lat_rad)**2 + ((1.0 - wgs84_f)*sin(lat_rad))**2)


def lon_lat_h_from_xyz(xyz_m):
    r'''
    *Example*
    >>> lon_lat_h_from_xyz([3727223.962, 655108.048, 5116998.574])
    (0.17398589340095263, 0.93721449998617889, 68.257986180484295)
    '''
    wgs84_a = 6378137.0                 # semi-major axis
    wgs84_inv_f = 298.257223563         # flattening
    wgs84_f = 1./298.257223563          # flattening
    wgs84_b = wgs84_a * (1.0 - wgs84_f) # semi-minor axis
    wgs84_e2 = wgs84_f* (2.0 - wgs84_f) # first eccentricity

    x_m, y_m, z_m = xyz_m
    lon_rad = arctan2(y_m, x_m)
    r_m = sqrt(x_m**2 + y_m**2)
    
    lat_previous = 10000.0
    lat_rad = arctan2(z_m, r_m)
    while abs(lat_rad - lat_previous) > 1e-12:
        lat_previous = lat_rad
        lat_rad = arctan2(z_m + wgs84_e2*wgs84_a*normalized_earth_radius(lat_rad)*sin(lat_rad),
                          r_m)
    height_m = r_m*cos(lat_rad) + z_m*sin(lat_rad) - wgs84_a*sqrt(1.0 - wgs84_e2*sin(lat_rad)**2)
    return lon_rad, lat_rad, height_m


def parse_pqr_file(file_name):
    lines = [line for line in open(file_name).readlines()
             if line.strip()!= '']
    fields = [field.strip() for field in lines[0].split(';') if field.strip() != '']
    rows = [dict([(fields[0], row.split(';')[0].strip())]+[(name, float(value)) for name, value in zip(fields[1:], row.split(';')[1:])])
            for row in lines[1:]]
    return rows



def point_kml(point_dict):
    point_fmt ='''   <Placemark>
    <name>%(name)s</name>
    <description>%(point)r</description>
    <Point>
      <coordinates>%(lon_deg)f,%(lat_deg)f,%(height_m)f</coordinates>
    </Point>
  </Placemark>'''
    lon_rad, lat_rad, height_m = lon_lat_h_from_xyz([point_dict['ETRS-X'], point_dict['ETRS-Y'], point_dict['ETRS-Z']])
    return point_fmt % {'name': point_dict['NAME'],
                        'point': point_dict,
                        'lon_deg': lon_rad*180/pi,
                        'lat_deg': lat_rad*180/pi,
                        'height_m': height_m}

def to_kml(points):
    fmt='''<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
%s
</kml>'''
    return fmt % ('\n'.join([point_kml(point) for point in points]),)


def point_csv(point_dict):
    lon_rad, lat_rad, height_m = lon_lat_h_from_xyz([point_dict['ETRS-X'], point_dict['ETRS-Y'], point_dict['ETRS-Z']])
    return '%f,%f,%f' % (lat_rad*180/pi, lon_rad*180/pi, height_m) 

def to_google_csv(points):
    return '\n'.join([point_csv(point) for point in points])
