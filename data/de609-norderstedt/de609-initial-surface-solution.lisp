(discarded-points
    NIL)
(reduced-chi-squared 51.20219468983136)
(normal-vector               (0.5950527 0.1049831 0.7968004))
(normal-vector-uncertainties (0.0000991 0.0001837 0.0000739))
(normal-vector-direction-uncertainties-arcsec :max 37.89 :rms 26.37 :avg 24.52)
(reference-point :etrs (3727257.656 655197.974 5116961.503)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.13936201872121723 -0.7915115134006666 0.5950527303459195)
        (0.9898909109504304 -0.09536531259968062 0.10498305373007885)
        (-0.026347906096586132 0.6036679396050048 0.7968003555078109)))
