(discarded-points
    NIL)
(reduced-chi-squared 0.459487425240932)
(normal-vector               (0.5982206 0.0719793 0.7980922))
(normal-vector-uncertainties (0.0000619 0.0000794 0.0000462))
(normal-vector-direction-uncertainties-arcsec :max 16.37 :rms 13.18 :avg 12.88)
(reference-point :etrs (3828733.422 454692.080 5063849.651)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11956212839387237 -0.7923616853194799 0.5982205756170551)
        (0.992826717762723 -0.09546774561477696 0.07197928899156025)
        (7.713900108388683e-5 0.6025353675801162 0.7980921781753464)))
