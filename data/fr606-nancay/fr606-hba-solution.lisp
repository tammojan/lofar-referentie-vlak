(discarded-points
    NIL)
(reduced-chi-squared 14.451737552170693)
(normal-vector               (0.6774164 0.0259243 0.7351428))
(normal-vector-uncertainties (0.0002012 0.0002724 0.0001852))
(normal-vector-direction-uncertainties-arcsec :max 56.18 :rms 45.96 :avg 45.29)
(reference-point :etrs (4324017.062 165545.130 4670271.065)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.0822885693771658 -0.7309826690000625 0.6774163630821185)
        (0.995776664154844 -0.08807249111846101 0.02592434054782762)
        (0.040711502975332464 0.6766886831695457 0.735142842987508)))
