(discarded-points
    NIL)
(reduced-chi-squared 16.207232057555185)
(normal-vector               (0.6771215 0.0255685 0.7354269))
(normal-vector-uncertainties (0.0002132 0.0002891 0.0001961))
(normal-vector-direction-uncertainties-arcsec :max 59.63 :rms 48.74 :avg 48.02)
(reference-point :etrs (4323980.263 165608.766 4670302.692)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.08209069216527405 -0.7312780544738056 0.6771214996622336)
        (0.9957827160144032 -0.08810808065514364 0.025568508174028552)
        (0.0409621867922707 0.6763648225390441 0.7354269005719892)))
