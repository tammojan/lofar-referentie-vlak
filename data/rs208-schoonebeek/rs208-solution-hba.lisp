(discarded-points
    NIL)
(reduced-chi-squared 61.57751759857488)
(normal-vector               (0.5980722 0.0719569 0.7982054))
(normal-vector-uncertainties (0.0000684 0.0000774 0.0000510))
(normal-vector-direction-uncertainties-arcsec :max 15.97 :rms 13.72 :avg 13.53)
(reference-point :etrs (3847752.750 466963.554 5048397.603)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11955945081741125 -0.7924741252402133 0.5980721516213555)
        (0.9928270399222863 -0.09548129294543217 0.07195687247664177)
        (8.074274051529663e-5 0.6023853281100289 0.7982054309251364)))
