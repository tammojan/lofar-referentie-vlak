(discarded-points
    NIL)
(reduced-chi-squared 789.9488334556056)
(normal-vector               (0.5989943 0.0695899 0.7977237))
(normal-vector-uncertainties (0.0006729 0.0016868 0.0005075))
(normal-vector-direction-uncertainties-arcsec :max 347.93 :rms 224.56 :avg 197.14)
(reference-point :etrs (3847773.081 466965.755 5048382.547)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11808604609387507 -0.7919984056171392 0.5989943332101104)
        (0.9930012641078172 -0.09542397583785231 0.06958990095965216)
        (0.002043330172152899 0.6030197263234484 0.7977236579584444)))
