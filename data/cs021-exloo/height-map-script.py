from pylab import *
from contourmap import *

antenna_stakes=[read_pqr(name, ',') for name in 'cs021-lba-stakes-pqr.csv cs021-hba-stakes-pqr.csv    '.split()]
if 'cs021-lba-stakes-pqr.csv cs021-hba-stakes-pqr.csv    ' is not '':
    antn=concatenate([n for n,p,q,r in antenna_stakes])
    antp=concatenate([p for n,p,q,r in antenna_stakes])
    antq=concatenate([q for n,p,q,r in antenna_stakes])
    antr=concatenate([r for n,p,q,r in antenna_stakes])
    antenna_positions=[antn, antp, antq, antr]
else:
    antenna_positions=[]
    pass

n,p,q,r = read_pqr('cs021-surface-pqr.csv')
ar = array(r)
sigma=ar.std()
ar_mean=ar.mean()
for i in range(10):
    new_sigma=ar[abs(ar-ar_mean)< 3*sigma].std()
    ar_mean=ar[abs(ar-ar_mean)< 3*sigma].mean()
    sigma=new_sigma
    pass
fig=plot_height_map_stats(p,q,r, 'cs021',standard_deviations=[int(sigma*1000 +0.5)/1000.0], antenna_positions=antenna_positions,resolution_m=2.0, mean_of_good_points=ar_mean,vmax=0.1)
savefig('cs021-height-map-stats.pdf',dpi=300, format='pdf',orientation='landscape')

fig=plot_height_map(p,q,r, 'cs021',standard_deviations=[int(sigma*1000 +0.5)/1000.0], antenna_positions=antenna_positions,resolution_m=2.0,vmax=0.1)
savefig('cs021-height-map.pdf',dpi=300, format='pdf',orientation='landscape')
