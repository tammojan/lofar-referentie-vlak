(discarded-points
    NIL)
(reduced-chi-squared 1.8872619295782578)
(normal-vector               (0.6272189 -.0143644 0.7787106))
(normal-vector-uncertainties (0.0003193 0.0003968 0.0002571))
(normal-vector-direction-uncertainties-arcsec :max 81.84 :rms 67.94 :avg 66.91)
(reference-point :etrs (4008462.277 -100376.946 4943716.595)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.0638712453160421 -0.7762196601855594 0.6272188638451435)
        (0.995513504219752 -0.09352287273949261 -0.01436437226830317)
        (0.06980921814407966 0.6234873787142504 0.7787105763029967)))
