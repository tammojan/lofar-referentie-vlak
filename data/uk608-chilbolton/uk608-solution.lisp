(discarded-points
    NIL)
(reduced-chi-squared 108.63768117772375)
(normal-vector               (0.6237985 -.0057423 0.7815641))
(normal-vector-uncertainties (0.0016639 0.0012585 0.0013279))
(normal-vector-direction-uncertainties-arcsec :max 343.19 :rms 294.50 :avg 292.23)
(reference-point :etrs (4008449.699 -100343.619 4943726.764)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.06974190905324341 -0.7784673933184995 0.6237985120706123)
        (0.995575095012682 -0.09379369100206282 -0.005742274697254393)
        (0.06297853850398796 0.6206377857237747 0.781564100134974)))
