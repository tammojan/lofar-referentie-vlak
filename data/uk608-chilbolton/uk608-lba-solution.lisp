(discarded-points
    NIL)
(reduced-chi-squared 2.064802237411224)
(normal-vector               (0.6269148 -.0147940 0.7789473))
(normal-vector-uncertainties (0.0003522 0.0003762 0.0002834))
(normal-vector-direction-uncertainties-arcsec :max 77.60 :rms 70.03 :avg 69.56)
(reference-point :etrs (4008438.427 -100309.565 4943735.860)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.06365517716220079 -0.7764829820347587 0.6269148243827526)
        (0.9955042318318389 -0.0935545990884314 -0.014793964793234509)
        (0.07013802695649077 0.6231546482212995 0.7789473291403207)))
