(discarded-points
    NIL)
(reduced-chi-squared 0.16741410980463287)
(normal-vector               (0.5982184 0.0721012 0.7980828))
(normal-vector-uncertainties (0.0000464 0.0000582 0.0000346))
(normal-vector-direction-uncertainties-arcsec :max 12.01 :rms  9.78 :avg  9.57)
(reference-point :etrs (3811650.301 453459.948 5076728.322)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11963470260404166 -0.7923523681607645 0.5982184071080318)
        (0.9928179780530775 -0.09546662303634522 0.07210122287891678)
        (-1.968353061143624e-5 0.6025477977356358 0.7980827971185032)))
