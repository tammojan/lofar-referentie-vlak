from pylab import *
from contourmap import *

antenna_stakes=[read_pqr(name, ',') for name in 'cs103-lba-stakes-pqr.csv     '.split()]
if 'cs103-lba-stakes-pqr.csv     ' is not '':
    antn=concatenate([n for n,p,q,r in antenna_stakes])
    antp=concatenate([p for n,p,q,r in antenna_stakes])
    antq=concatenate([q for n,p,q,r in antenna_stakes])
    antr=concatenate([r for n,p,q,r in antenna_stakes])
    antenna_positions=[antn, antp, antq, antr]
else:
    antenna_positions=[]
    pass

n,p,q,r = read_pqr('cs103-lba-surface-pqr.csv')
ar = array(r)
sigma=ar.std()
for i in range(10):
    sigma=ar[abs(ar)< 3*sigma].std()
    pass
fig=plot_height_map_stats(p,q,r, 'CS103 - exloo',standard_deviations=[int(sigma*1000 +0.5)/1000.0], antenna_positions=antenna_positions)
savefig('cs103-lba-height-map-stats.pdf',dpi=300, format='pdf',orientation='landscape')

fig=plot_height_map(p,q,r, 'CS103 - exloo',standard_deviations=[int(sigma*1000 +0.5)/1000.0], antenna_positions=antenna_positions)
savefig('cs103-lba-height-map.pdf',dpi=300, format='pdf',orientation='landscape')
