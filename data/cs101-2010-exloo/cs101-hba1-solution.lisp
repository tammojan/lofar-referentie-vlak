(discarded-points
    NIL)
(reduced-chi-squared 0.19051356290716767)
(normal-vector               (0.5988306 0.0722612 0.7976091))
(normal-vector-uncertainties (0.0000856 0.0001171 0.0000640))
(normal-vector-direction-uncertainties-arcsec :max 24.16 :rms 18.88 :avg 18.34)
(reference-point :etrs (3825900.669 461698.069 5065338.765)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11968599765407806 -0.7918820802892746 0.5988305543993884)
        (0.992811791945635 -0.09540996037368377 0.07226122912869277)
        (-8.797298089244232e-5 0.6031746930850322 0.7976091034350924)))
