(discarded-points
    NIL)
(reduced-chi-squared 0.7416771488469652)
(normal-vector               (-.0000810 0.0003657 0.9999999))
(normal-vector-uncertainties (0.0000518 0.0000502 .00000002))
(normal-vector-direction-uncertainties-arcsec :max 10.68 :rms  8.59 :avg  7.01)
(reference-point :etrs (44.500 67.000 0.452)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11961992651123428 -0.9928197553528892 -8.100911028196173e-5)
        (0.9928196886934842 -0.11961994831582319 3.656603517129043e-4)
        (-3.727251265148962e-4 -3.668717525150368e-5 0.9999999298650132)))
