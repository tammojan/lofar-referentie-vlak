(discarded-points
    NIL)
(reduced-chi-squared 0.35053118151682056)
(normal-vector               (0.5987298 0.0720966 0.7976997))
(normal-vector-uncertainties (0.0000131 0.0000130 0.0000098))
(normal-vector-direction-uncertainties-arcsec :max  2.70 :rms  2.49 :avg  2.47)
(reference-point :etrs (3829798.382 452812.012 5063218.695)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11959537300862715 -0.7919719896781173 0.5987297506556788)
        (0.9928227161396246 -0.0954207931118404 0.07209664734955334)
        (3.2742416671990855e-5 0.6030549227120124 0.7976996672437208)))
