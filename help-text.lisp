#||

\section{On-line help texts}

||#
(in-package :lofar-reference-plane)
#||

I first specify the help texts for all commands. When that is done,
the commands will be implemented.

||#

(defparameter *help*
  '((help
     "Usage: statcor <command> <options>

This program implements the relations between local station
coordinates and ETRS89 coordinates. One can fit a plane to a set of
ETRS coordinates, or, given information about the plane of a station,
convert back and forth between ETRS89 and station PQR cordinates.

Commands
--------
    help <command-name>  Display documentation of <command-name>.
    version              Display version information.
    fit-plane            Fit a plane through a set of ETRS89 coordinates.
    etrs-to-pqr          Convert ETRS coordinates to station PQR.
    pqr-to-etrs          Convert station PQR coordinates to ETRS.
    angle                Compute the angle between two normal vectors.
    project              Project a point onto a plane (parallel to the core)

Author: Michiel Brentjens <brentjens@astron.nl> (2008)
")


    (fit-plane
     "Usage: statcor fit-plane <options> <input-data-file>

Fit a plane through a set of ETRS89 points. If no input file is
specified, the data are read from standard input. If no output file is
specified, the data are written to standard output. The input data
should be in ASCII form with columns separated by either spaces, or
semicolons, for example:
----------------------------------------------------------
Name;ETRS-X;ETRS-Y;ETRS-Z;Uncertainty
10000;3826597.2075653;460842.23827549;5064896.5670354;0.06
10001;3826609.0108658;460843.95827016;5064887.4821479;0.06
10002;3826642.9608768;460856.32348526;5064860.9124205;0.06
P0;3826663.5405048;460870.33948328;5064844.2343701;0.06
P1;3826665.7594512;460871.56419892;5064842.439327;0.06
P2;3826682.6301798;460888.93833183;5064828.225256;0.06
P4;3826689.9034521;460898.99945718;5064821.8473183;0.06
P5;3826697.2225018;460910.24290331;5064815.2872654;0.06
-----------------------------------------------------------
It is forbidden to make spaces or semicolons part of a name (column or
point). The uncertainty column is optional. If it is not in the file,
one should  provide a default uncertainty using the --uncertainty
argument.

Options
-------
    -u/--uncertainty <x> Specify a default uncertainty to be used if no
                         uncertainty is provided in the input file.
    -o/--output <name>   Specify the name of the output file that will
                         contain a summary of the fit. This summary
                         includes:
                          - A list of discarded points
                          - The reduced chi-squared of the fit
                          - The standard deviation of the residuals
                          - The resulting normal vector including its
                            uncertainties.
                         The output file has the following layout:
----------------------------------------------------------
(discarded-points (\"P0;3826663.5405048;460870.33948328;5064844.2343701;0.06\"
                   \"P1;3826665.7594512;460871.56419892;5064842.439327;0.06\"))
(reduced-chi-squared 1.5123)
(normal-vector (0.598753 0.072099 0.797682))
(normal-vector-uncertainties (0.000042 0.000055 0.000032))
(normal-vector-direction-uncertainties-arcsec :max 11.50 :rms 9.19 :avg 8.89)
(reference-point :etrs        (3826689.903 460898.999 5064821.847)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
  #2A((-0.11961994839632437 -0.9928197560210317  0)
      (0.9928197560210317  -0.11961994839632437 0)
      (0                    0                   1)))
----------------------------------------------------------
                         The output file is designed such that it can
                         be used as a coordinate system specification
                         file for the commands etrs-to-pqr and
                         pqr-to-etrs.
    -l/--input-lon-lat   Read WGS84 ETRS89 coordinate file provided by
                         Azimuth B.V. This file should have the following
                         format:
                         ID dd mm ss.sss N d mm ss.sss E hh.hhh NAP")
    (etrs-to-pqr
     "Usage: statcor etrs-to-pqr <options> plane-spec-file <input-file>

Convert ETRS coordinates in the optional <input-file> to station PQR
coordinates using the plane defined by the normal-vector and
reference-point in the required plane-spec-file. One can use the
output file of the fit-plane command for this. The input file should
have the same structure as in the fit-plane command.

This command produces a CSV file with the following structure:
-------------------------------------------------------------
Name;ETRS-X;ETRS-Y;ETRS-Z;STATION-P;STATION-Q;STATION-R
P0;3826663.5405048;460870.33948328;5064844.2343701;0.0;2.4;0.0
-------------------------------------------------------------

Options
-------
    -o/--output <name>   Specify the name of the output file. If this
                         is not specified, read from standard input.
    -l/--input-lon-lat   Read WGS84 ETRS89 coordinate file provided by
                         Azimuth B.V. This file should have the following
                         format:
                         ID dd mm ss.sss N d mm ss.sss E hh.hhh NAP")
    (pqr-to-etrs
     "Usage: statcor pqr-to-etrs <options> plane-spec-file
     <input-file>

Convert station PQR coordinates in the optional <input-file> to ETRS89
coordinates using the plane defined by the normal-vector and
reference-point in the required plane-spec-file. One can use the
output file of the fit-plane command for this. The input file should
have the following structure:
---------------------------------------------------------------
Name;STATION-P;STATION-Q;STATION-R
P0;12.0;2.4;0.01
---------------------------------------------------------------
The output file has the following structure:
-------------------------------------------------------------
Name;ETRS-X;ETRS-Y;ETRS-Z;STATION-P;STATION-Q;STATION-R
P0;3826663.5405048;460870.33948328;5064844.2343701;12.0;2.4;0.01
-------------------------------------------------------------

Options
-------
    -o/--output <name>   Specify the name of the output file. If this
                         is not specified, read from standard input.
    -f/--format <format> The format may either be 'csv' (default) or
                         'totalstation' (for automatic input for the
                         totalstation employed by Azimuth. Each line
                         has the following format in that case:
                         number, lat (dd.mmss(.)sss), lon(dd.mmss(.)sss), GRS80 height (m), remark (optional)
                         Example:
                         9055,53.2040153,6.5816397,42.252,CLBA.
                         Means:
                           - ID 9055,
                           - LAT 53 deg, 20\', 40.153\",
                           - LON 6 deg, 58\', 16.397\",
                           - HEIGHT 42.252 above WGS84 ellipsoid
                           - NAME CLBA
    -b/--base-number <n> First point ID in output file if the output
                         file is in totalstation format. Defaults to 1001.
                         A better option is to start at 6001 for the LBA
                         stakes and at 6501 for the HBA stakes.")
    (version
    "Usage: statcor version

Display version information.")
    (angle
     "Usage: statcor angle plane-spec-file1 plane-spec-file2

Compute the angle between the normal vectors given in plane-spec-file1
and plane-spec-file2. These files must each contain one entry of the
form:

(normal-vector (0.598753 0.072099 0.797682))

The files may contain other entries too, but those will be
ignored. The result is written to standard output.")
    (project
     "Usage: statcor project <options> \"coordinates\"

Project 3D ETRS coordinates to a plane defined by a reference point
and a normal vector. If no explicit normal vector is provided, the
normal vector to the NAP plane at the centre of the LOFAR superterp is
used. It is required to specify a reference point. A position is
specified by three floating point numbers enclosed in quotes, for
example:

bash ~$ statcor project -r \"3837951.547 449596.035 5057370.512\" \\\\
\"3837893.127 449551.008 5057417.817\"
3837893.421 449551.043 5057418.209
bash ~$

Options
-------
    -r/--reference \"<coordinates>\" Specify a reference position in
                                     the plane.
    -n/--normal \"<normal-vector>\"  Specify an explicit normal vector.")
))

