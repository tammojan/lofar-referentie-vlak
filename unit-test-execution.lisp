(in-package :lofar-reference-plane)

#||

Execute unit tests.

||#


(eval-when (:load-toplevel)
  (unit-test::run-tests-with-report :packages '(:lofar-reference-plane)))