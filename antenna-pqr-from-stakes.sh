#!/bin/bash

LBA_STAKES_NAME=$1
OUTPUT_FILE=$2

echo "NAME,STATION-P,STATION-Q,STATION-R" > $OUTPUT_FILE
grep -e "^M" $LBA_STAKES_NAME |sed -e 's/M//g'|sed -e 's/,/ /g'|sort -n|sed -e's/^\(.*\)$/L\1/g'|awk '/.*/ {print $1,$2+0.06,$3+0.06,$4}'|sed -e's/ /,/g' >>$OUTPUT_FILE
