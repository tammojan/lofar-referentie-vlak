.PHONY: doc

SOURCES=make.lisp reference-plane.lisp reference-plane.asd utilities.lisp main.lisp csv-txt-input.lisp package.lisp unit-test-execution.lisp

all: $(SOURCES) Makefile
	sbcl --no-userinit --load make.lisp
doc:
	cllit && make -C doc/
