(discarded-points
    NIL)
(reduced-chi-squared 0.9004497128508949)
(normal-vector               (0.5987542 0.0721011 0.7976809))
(normal-vector-uncertainties (0.0000019 0.0000031 0.0000014))
(normal-vector-direction-uncertainties-arcsec :max  0.65 :rms  0.47 :avg  0.44)
(reference-point :etrs (3824110.416 459442.859 5066883.104)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11959625573171294 -0.7919533354643437 0.5987542484708193)
        (0.9928226098446035 -0.09541854555776479 0.07210108560202984)
        (3.156430249038473e-5 0.6030797754945373 0.7976808781669229)))
