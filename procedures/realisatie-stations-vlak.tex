\documentclass[a4paper,10pt,twoside]{article}

\usepackage{graphicx}

\title{Realisatie van het het stationsvlak evenwijdig aan de superterp.}
\author{Michiel Brentjens\\
  Corina Vogt\\
  Hans van der Marel\\
  Arie Huijgen}

\newcommand{\columnvector}[1]{\left(\begin{array}{r}#1\end{array}\right)}
\newcommand{\rowvector}[1]{\left(#1\right)}



\begin{document}

\maketitle

{\huge \textbf{D R A F T}}

\section{Doel}

Dit document beschrijft de te volgen meetmethoden om het grondvlak van
een station evenwijdig te krijgen aan dat van de superterp bij
Exloo. Binnen het core-gebied kan van het NAP waterpasvlak worden
uitgegaan. Daarbuiten is dat geen geldige benadering meer en dient de
procedure die hier beschreven wordt te worden gevolgd.

\section{Definities en conventies}

\begin{description}
\item[Referentievlak] Het vlak evenwijdig aan het NAP vlak ter plaatse
  van de superterp.
\item[Grondvlak] Definitieve vlak van het maaiveld van een station na
  vlakken.
\item[\emph{Core station}] Station in het centrale LOFAR gebied met
  dubbel HBA station.
\item[\emph{Remote station}] Alle overige Nederlandse stations.
\end{description}


\section{Kleuren}

Piketten die het referentievlak aangeven dienen oranje te worden
geverfd. Piketten die stationscontouren aangeven dienen wit te worden
geverfd. Contourpiketten die op de hoogte van het referentievlak zijn
gezet krijgen een oranje kop met wit daar onder.


\section{Procedure in hoofdlijnen}

De procedure bestaat uit drie hoofdfasen:
\begin{enumerate}
\item Uitzetten van een vlak evenwijdig aan het referentievlak door
  middel van piketten.
\item Vlakken van de grond evenwijdig aan dit referentievlak.
\item Opmeten van het gerealiseerde grondvlak.
\end{enumerate}

Fase 1 dient te worden gedaan voor alle Nederlandse \emph{remote
  stations}. Indien hierna na beoordeling ter plekke is vastgesteld
dat het binnen redelijke grenzen mogelijk is om het grondvlak
daadwerkelijk evenwijdig te maken aan het referentievlak, wordt fase 2
ook uitgevoerd. Indien anders is beslist wordt het station zodanig
gevlakt dat er minimaal grondverzet nodig is. Bij fase 2 is het in het
bijzonder van belang dat de laser de correcte helling aangeeft.  Fase
drie zal ook voor alle stations moeten gebeuren.

De meetresultaten van stappen 1 en 3 dienen met mm precisie te worden
opgeslagen en naar ASTRON te worden verzonden.


\section{Fase 1: Piketten uitzetten}

Voordat de piketten worden uitgezet dient het veld eerst te worden
verkend. Eventueel dient de toplaag met begroeiing te worden
verwijderd \emph{voordat} de piketten worden uitgezet.

De piketten worden uitgezet met behulp van een totaalstation.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{reference-plane-stakes}
  \caption{Voorbeeldopstelling van de piketten die het referentievlak
    aangeven. De rechthoek geeft het gebied aan dat gevlakt moet
    worden. Er moet tenminste \'e\'en rechte hoek zitten in de
    opstelling van de piketten om bij het vlakken de laser neer te
    kunnen zetten en uit te lijnen. In dit geval is dat P\_3.}
  \label{fig:stake-plan}
\end{figure}


\subsection{Eerste piket}

\begin{figure}
\includegraphics[width=\textwidth]{empty-spreadsheet}
\includegraphics[width=\textwidth]{rs106-spreadsheet}
\caption{De spreadsheet die bij het uitzetten van de stationspiketten
  moet worden ingevuld. Leeg (boven) en deels ingevuld voor RS106 (onder).}
\label{fig:spreadsheet}
\end{figure}

\begin{enumerate}
\item Installeer en calibreer het totaalstation.
\item Sla een piket ergens ruwweg in het midden van het te vlakken
  gedeelte. Dit piket moet afhankelijk van de geschatte ori\"entatie en
  kromming van het veld 10 tot 60~cm boven het maaiveld steken.
\item Meet de ETRS89 $X$, $Y$ en $Z$ co\"ordinaten van het midden van de kop
  van dit piket. Dit is de eerste positie, P\_0, in het vlak.
\item Voer de coordinaten van P\_0 in in de spreadsheet (Fig.~\ref{fig:spreadsheet}).
\item Markeer de kop van het piket met oranje verf.
\end{enumerate}




\subsection{Alle volgende piketten}

De overige piketten moeten minstens een meter en bui voorkeur 3 meter
buiten de rand van het te vlakken gedeelte worden geslagen. Er dienen
ten minste acht piketten te worden geslagen langs de rand: vier op de
hoeken en vier halverwege de zijden van het veld. Zoals te zien in
Fig.~\ref{fig:stake-plan}, moeten er tenminste vijf piketten samen een
rechte hoek vormen (P\_1--P\_5 in Fig.~\ref{fig:stake-plan}). Er
kunnen natuurlijk altijd meer piketten worden geslagen als dat door
lokale omstandigheden nodig blijkt.


De procedure per piket is als volgt:
\begin{enumerate}
\item Loop naar de plek waar piket P\_$i$ moet komen ($i>0$). \item
  Bepaal de ETRS89 $X$, $Y$ en $Z$ positie van een willekeurig punt op
  de grond op deze lokatie. Dit punt heet $i$.
\item Projecteer het punt $i$ op het referentievlak met behulp
  van de door ASTRON geleverde spreadsheet. Dit wordt punt P\_$i$.
\item Sla nu een piket zodanig in de grond dat het midden van de kop
  ruim binnen 1~cm van positie P\_$i$ zit.
\item Verf de bovenkant van het piket oranje.
\end{enumerate}




\subsection{Gegevens voor  ASTRON}

De volledig ingevulde spreadsheet kan naar ASTRON worden
verzonden. Doorgaans zal er echter al iemand van ASTRON bij het
uitzetten aanwezig zijn. Diegene is dan verantwordelijk voor het
correct invullen van de spreadsheet.



\section{Fase 2: Installeren van de laser}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{example-map}
  \caption{Voorbeeld van een kaart zoals die door ASTRON aan degene
    die het veld gaat vlakken geleverd wordt.}
  \label{fig:flattening-map}
\end{figure}


\subsection{Door ASTRON geleverde gegevens}

ASTRON maakt een 1:2000 kaart van het te vlakken veld met daarop
aangegeven tenminste de volgende zaken:
\begin{itemize}
\item De posities van de piketten die het referentievlak aangeven.
\item Het geografische noorden.
\item Het te vlakken gebied.
\item De piket paal waarachter de laser moet worden opgesteld
  (omcirkeld in Fig.~\ref{fig:flattening-map}).
\item De afstanden van belangrijke piketten tot de piket waarachter de
  laser zal worden opgesteld.
\end{itemize}



\subsection{Positie van de laser}

\begin{figure}
  \centering
  \begin{minipage}{0.47\textwidth}
  \centering
  \includegraphics[width=\textwidth]{laser-position}
  \caption{Positie van de laser ten opzichte van de piketpaal
    waarachter hij wordt opgesteld.}
  \label{fig:laser-position}
  \end{minipage}
  \hspace{\fill}
  \begin{minipage}{0.47\textwidth}
  \centering
  \vspace{12mm}
  \includegraphics[width=\textwidth]{laser-calibration-horizontal}
  \caption{The helling wordt ingesteld met behulp van een ontvanger op
    een uitschuifbare staf.}
  \label{fig:laser-calibration}
  \end{minipage}
\end{figure}

De laser wordt dicht bij een van de piketpaaltjes opgesteld
(Fig.~\ref{fig:laser-position}). De kop van de laser moet ten minste
10~cm en hooguit 50~cm van de verticale lijn door de kop van de piket
staan. De X as van de laser moet evenwijdig aan de ene zijde van het
veld staan en de Y as evenwijdig aan de andere.


\subsection{Instellen van de helling}

De helling van de laser wordt als volgt ingesteld:
\begin{enumerate}
\item Laat de laser zichzelf waterpas zetten.
\item Monteer de ontvanger op een uitschuifbare staf.
\item Zet de staf verticaal op de piketpaal en schuif hem uit totdat de
  ontvanger in de laserbundel zit  (Fig.~\ref{fig:laser-calibration}).
\item Markeer dit punt op de staf.
\item Loop naar het meest ver weg gelegen punt langs een van de assen
  van de laser (bijvoorbeeld P1).
\item Zet de staf op het piketpaaltje en verstel de lengte totdat de
  ontvanger in de laserbundel zit.
\item Noteer het hoogteverschil in cm (+ betekent ``de staf is hier
  langer dan bij de laser'', $-$ betekent ``de staf is hier korter dan
  bij de laser'').
\item Loop terug naar de laser en herhaal punt 6 en 7 bij het paaltje
  halverwege (bijvoorbeeld P2 in Fig.~\ref{fig:laser-calibration}).
\item Bereken de gewogen gemiddelde helling $s$ in procenten als volgt:
  \begin{equation}
    s = \frac{h_1\ [\mbox{cm}] + h_2\ [\mbox{cm}]}{d_1\ [\mbox{m}] + d_2\ [\mbox{m}]},
  \end{equation}
  waar $h_1$ en $h_2$ de gemeten hoogteverschillen t.o.v. het paaltje
  bij de laser zijn in cm en $d_1$ en $d_2$ de afstanden van de
  piketten tot de piketpaal bij de laser in m. Deze afstanden staan op
  het kaartje vermeld.
\item Voer deze helling in bij de juiste as van de laser. Let op of
  het teken eventueel van + naar $-$ veranderd moet worden (of
  omgekeerd) in verband met de conventie die de laser aanhoudt.

\item Ga terug naar de paaltjes en controleer dat de staf niet meer
  dan 1~cm per paaltje hoeft te worden verschoven t.o.v. de hoogte bij
  de laser. Indien het verschil groter is, kunnen punten 5 t/m 11
  worden herhaald om een eventuele extra correctie te
  bepalen. Controleer het resultaat net zolang totdat je bij beide
  piketten binnen 1~cm goed zit.
\item Herhaal stappen 5 t/m 12 voor de piketten op de as loodrecht op
  de zojuist ingestelde as.
\item Controleer voor de zekerheid dat de andere piketten in het veld
  ook binnen 1~cm van het vlak zitten.
\end{enumerate}

De laser is nu correct ingesteld. Het vlakken kan beginnen.


\section{Fase 3: Verificatie van het gerealiseerde vlak}

Nadat een veld volledig is gevlakt moet het oppervlak opgemeten worden
om te controleren of het de juiste helling heeft en of het voldoende
vlak is. Om dit vast te stellen is het nodig om op enkele honderden
punten van het veld de ETRS89 $X$, $Y$ en $Z$ coordinaten van het
oppervlak te meten. De punten dienen goed gespreid over het hele veld
opgemeten te worden. De meetafstand op het LBA veld dient 5 normale
wandelstappen te zijn en op het HBA veld 3 normale wandelstappen. Dir
resulteert als het goed is in tussen de 600 en 800 meetpunten. De
gemeten co\"ordinaten kunnen als digitaal CSV bestand of Excel
spreadsheet naar ASTRON gestuurd worden.



\appendix

\section{Wiskunde}

Vectoren worden als $\vec{c}$ genoteerd. Alle vectoren zijn ten
opzichte van het Cartesische ETRS89 systeem in het ETRF2005
\emph{framework}:
\begin{equation}
\vec{c} = \columnvector{c_x\\c_y\\c_z}.
\end{equation}
Alle lengtes zijn in meters. Resultaten dienen tot op millimeters te worden genoteerd:
\begin{equation}
  \vec{p}_0 = \columnvector{3837951.547\\449596.035\\5057370.512}.
\end{equation}

De projectie van een punt op de grond naar het referentie vlak  gebeurt volgens de volgende formule:
\begin{equation}
  \vec{p}_i =  \vec{a}_i - \left[\vec{n}\cdot\left(\vec{a}_i - \vec{p}_0\right)\right]\vec{n},
\end{equation}
waar $\vec{n}$ de normaalvector van het gewenste grondvlak is:
\begin{equation}
  \vec{n} = \vec{n}_\mathrm{ref} =\rowvector{0.598753\ 0.072099\ 0.797682}
\end{equation}
en $\cdot$ het standaard vector inproduct:
\begin{equation}
  \vec{a}\cdot\vec{b} = a_xb_x + a_yb_y + a_zb_z.
\end{equation}
Het is essentieel dat deze berekening in het veld gedaan kan
worden. Eventueel met een spreadsheet of het Mac OS X programma van
Michiel Brentjens. Rekenvoorbeeld van station Witterveen (RS307):
\begin{eqnarray}
  \vec{p}_0 & = &
  \rowvector{3837951.547\ \ 449596.035\ \ 5057370.512}\\
  \vec{a}_1 & = & 
  \rowvector{3837893.127\ \ 449551.008\ \ 5057417.817}\\
  \vec{p}_1 & = & 
  \rowvector{3837893.421\ \ 449551.043\ \ 5057418.209}.
\end{eqnarray}



\vspace{3cm}

\textbf{END OF DOCUMENT}
\end{document}