\documentclass[a4paper,11pt]{article}

\usepackage{graphicx}

\title{Establishing the reference ground plane in a LOFAR station}
\author{Michiel Brentjens\\
  Corina Vogt\\
  Hans van der Marel\\
  Arie Huijgen}

\newcommand{\columnvector}[1]{\left(\begin{array}{r}#1\end{array}\right)}
\newcommand{\rowvector}[1]{\left(#1\right)}



\begin{document}

\maketitle

{\huge D R A F T}

\section{Introduction}

It is advantageous for LOFAR calibration and imaging, if as many
stations as possible have parallel ground planes. For the core
stations and stations in the first two rings this is mandatory. For
other stations one should attempt to make them as parallel to the core
as possible within the financial and physical constraints of the
location. 

The reference ground plane will be constructed at a given station
using a set of wooden stakes. The ground level should be made parallel
to the plane through the tops of these stakes. Note that it is
important that the ground plane of a station is \emph{parallel} to
this reference plane. The \emph{absolute height} above (or below) ground
level is unimportant. The contracter who will flatten the field is
free to choose an absolute height that is convenient in terms of the
amount of soil that must be displaced and in terms of the local water
management.

This document describes a procedure to establish the reference ground
plane in the field.

Vectors are typeset as $\vec{c}$. All vectors are with repect to the
ETRS89 cartesian coordinate system and have units of meters. The
vector
\begin{equation}
\vec{c} = \columnvector{c_x\\c_y\\c_z}.
\end{equation}
All positions should be recorded down to the mm, for
example
\begin{equation}
  \vec{p}_0 = \columnvector{3837951.547\\449596.035\\5057370.512}.
\end{equation}



\section{Installing the wooden stakes}

The positions of the stakes are measured with the help of a total
station. It is important that the field is surveyed and, if needed,
the top layer is removed, before the reference plane stakes are
installed.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{reference-plane-stakes}
  \caption{Example layout of the stakes that indicate the reference
    plane. The dashed rectangle indicates the area that must be
    flattened. The stake configuration should have at least one
    90$^\circ$ corner, in this case marked P3, in which the laser can
    be set up for the flattening procedure.}
  \label{fig:stake-plan}
\end{figure}

\subsection{The first stake}

\begin{enumerate}
\item Install the total station somewhere in the field and determine
  its position;
\item Compute a centre position somewhere in the field. This may be
  the centre of the LBA field, or the average position of the LBA and
  HBA field centres, depending on whether one is working on a core
  station or on a remote station respectively. This is best done in a
  local coordinate system such as RD/NAP in the
  Netherlands. Alternatively, one can walk to what appears to be the
  approximate center without any precomputations;
\item Insert a stake into the ground at this position. Make sure that the
  height above the current ground level of this stake is such that the
  minimum height above local ground level of all planned stakes is
  more than $\approx 15$~cm. In a more-or less horizontal field, 30~cm
  is a reasonable height.
\item Measure the ETRS89 $X$, $Y$, and $Z$ coordinates of the top of this
  central stake. This will be the first position in the plane. We call
  this position $\vec{p}_0$. Record the coordinates of this position;
\item Mark the top of the stake with pink paint.
\end{enumerate}



\subsection{The other stakes}

The other stakes should be installed approximately one meter outside
the edge of the area that must be flattened. The minimum would be four
stakes at the corners of the area with it least one extra stake in two
perpendicular sides. See Fig.~\ref{fig:stake-plan} for reference.  Of
course, more stakes are allowed if deemed necessary by local
circumstances.

\begin{enumerate}
\item Walk to the approximate location of stake number $i$, with $i >
  0$;
\item Determine the ETRS89 $X$, $Y$, and $Z$ coordinates of a
  convenient point on the ground. For stake number $i$ this will be
  the approximate location $\vec{a}_i$. Record the ETRS89 coordinates
  of this position;
\item Project the point $\vec{a}_i$ onto the reference plane to
  determine the position $\vec{p}_i$ using the following formula:
  \begin{equation}
    \vec{p}_i =  \vec{a}_i - \left[\vec{n}\cdot\left(\vec{a}_i - \vec{p}_0\right)\right]\vec{n},
  \end{equation}
  where $\vec{n}$ is the normal vector of unit length of the desired
  ground plane, and ``$\cdot$'' indicates a standard vector inner
  product:
  \begin{equation}
    \vec{a}\cdot\vec{b} = a_xb_x + a_yb_y + a_zb_z.
  \end{equation}
  Record the ETRS89 coordinates of this position.  For the stations
  that will be parallel to the LOFAR core,
  \begin{equation}
    \vec{n} = \vec{n}_\mathrm{ref} =\rowvector{0.598753\ 0.072099\ 0.797682}.
  \end{equation}
  An example from the station near Witterveen:
  \begin{eqnarray}
    \vec{p}_0 & = &
    \rowvector{3837951.547\ 449596.035\ 5057370.512}\\
    \vec{a}_1 & = & 
    \rowvector{3837893.127\ 449551.008\ 5057417.817}\\
    \vec{p}_1 & = & 
    \rowvector{3837893.421\ 449551.043\ 5057418.209}.
  \end{eqnarray}
  It is essential that this computation is done \emph{in the
    field}. You can either use the Mac OS X software utility written
  by Michiel Brentjens, or implement the equation yourself on whatever
  system is convenient to use in the field;
\item Determine the point on the ground that is right below $\vec{p}_i$
  with the help of the total station;
\item Insert a stake into the ground at this position and drive it
  down until the total station indicates that it is at the requested
  position. The position is good enough if the total station indicates
  that it is within one cm of the requested position;
\item Mark the top of the stake with pink paint.
\end{enumerate}



\subsection{Data to provide to ASTRON}

ASTRON would like to receive the ETRS89 coordinates of the centre
stake position, and the approximate and final positions of each
stake. These positions can for example be supplied as a digital CSV
file with the following layout:

\begin{verbatim}
Name;ETRS-X     ;ETRS-Y    ;ETRS-Z
"P0";3837951.547;449596.035;5057370.512
"A1";3837893.127;449551.008;5057417.817
"P1";3837893.421;449551.043;5057418.209
"A2";3837927.158;449517.577;5057395.646
"P2";3837927.284;449517.592;5057395.814
...
...
\end{verbatim}

In addition we would like to receive the following auxilliary data:

\begin{itemize}
\item Name of the station;
\item Date and approximate start and end time of the measurements;
\item Names of people present during the measurements.
\end{itemize}



\section{Installing the laser}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{example-map}
  \caption{Example of a map that will be provided by ASTRON to the
    field flattening crew.}
  \label{fig:flattening-map}
\end{figure}


\subsection{Data provided by ASTRON}

ASTRON will provide a map of the field (scale 1:1000) indicating at
least:
\begin{itemize}
\item the positions of the wooden stakes that indicate the reference
  plane;
\item geographic north;
\item the area that must be flattened;
\item the stake behind which the laser must be installed (encircled in
  Fig.~\ref{fig:flattening-map});
\item the distances between the stake ear the laser and all other
  stakes.
\end{itemize}

An example is shown in Fig.~\ref{fig:flattening-map}.



\subsection{Position}

\begin{figure}
  \centering
  \begin{minipage}{0.47\textwidth}
  \centering
  \includegraphics[width=\textwidth]{laser-position}
  \caption{Position of the laser with respect to the stake near which
    it must be installed.}
  \label{fig:laser-position}
  \end{minipage}
  \hspace{\fill}
  \begin{minipage}{0.47\textwidth}
  \centering
  \vspace{12mm}
  \includegraphics[width=\textwidth]{laser-calibration-horizontal}
  \caption{The inclination of the laser is calibrated with the help of
  an adjustable staff to which the receiver is mounted.}
  \label{fig:laser-calibration}
  \end{minipage}
\end{figure}

The laser shall be installed close to one of the wooden stakes. The
head of the laser should be at least 10~cm and at most 50~cm away from
the vertical line through the stake. See
Fig.~\ref{fig:laser-position}. The X axis of the laser should point
parallel to one of the sides of the field, and the Y axis parallel to
the perpendicular side.


\subsection{Calibration}


The inclination of the laser is adjusted as follows:
\begin{enumerate}
\item let the laser set itself level;
\item take an adjustable staff and mount the receiver on the staff;
\item hold the staff vertically on top of the stake near the laser and
  adjust length until the receiver is in the plane of the laser
  (Fig.~\ref{fig:laser-calibration});
\item mark this point on the staff;
\item walk along one of the axes towards the closest stake (P2 for
  example);
\item put the staff on top of the stake and adjust the staff until the
  receiver is in the plane of the laser;
\item write down height difference in cm (+ means ``staff is longer here
  than near laser'', $-$ means ``staff is shorter here than near
  laser'');
\item walk to most distant marker along this axis (for example P1) and
  measure height difference relative to the stake near the laser;
\item write down height difference in cm;
\item compute the weighted average slope $s$ in percent as follows:
  \begin{equation}
    s = \frac{h_1\ [\mbox{cm}] + h_2\ [\mbox{cm}]}{d_1\ [\mbox{m}] + d_2\ [\mbox{m}]},
  \end{equation}
  where $h_1$ and $h_2$ are the heigh differences in cm and $d_1$ and
  $d_2$ are the distances between the stakes and the stake near the
  laser in m as indicated in the map;
\item enter the slope $s$ in the laser;
\item verify that the slope is now correct, that is, that the stakes
  (P2 and P3 in this case) are within 1~cm of the laser plane. If they
  are not, repeat steps 5 to 11 in order to determine the additional
  correction and re-verify;
\item repeat steps 5 to 12 for the other axis using the stakes on the
  perpendicular side of the field (for example P4 and P5).
\item verify that the other stakes in the field are all within 1~cm of
  the laser plane.
\end{enumerate}

The laser is now properly aligned.

\vspace{3cm}

\textbf{END OF DOCUMENT}
\end{document}
