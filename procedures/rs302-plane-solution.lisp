(discarded-points
    NIL)
(reduced-chi-squared 0.37069955375862434)
(normal-vector               (0.5987540 0.0721011 0.7976811))
(normal-vector-uncertainties (0.0000007 0.0000009 0.0000005))
(normal-vector-direction-uncertainties-arcsec :max  0.19 :rms  0.15 :avg  0.15)
(reference-point :etrs (3827925.020 459804.333 5064004.378)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11959626505697485 -0.7919535112665451 0.5987540140800829)
        (0.9928226087216687 -0.09541856673930368 0.07210107303305213)
        (3.1551898315121396e-5 0.6030795412829117 0.7976810552410472)))
