(discarded-points
    NIL)
(reduced-chi-squared 0.15827736688130142)
(normal-vector               (0.5987540 0.0720965 0.7976815))
(normal-vector-uncertainties (0.0000016 0.0000031 0.0000012))
(normal-vector-direction-uncertainties-arcsec :max  0.63 :rms  0.44 :avg  0.40)
(reference-point :etrs (3829250.622 469159.764 5062145.763)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11959352794489628 -0.7919539211235161 0.5987540186844519)
        (0.9928229383101321 -0.09541861612094567 0.07209646914238792)
        (3.5198423250545896e-5 0.6030789952524057 0.7976814679096036)))
