(discarded-points
    NIL)
(reduced-chi-squared 7.614158347869076)
(normal-vector               (0.5987536 0.0720998 0.7976815))
(normal-vector-uncertainties (0.0000015 0.0000016 0.0000011))
(normal-vector-direction-uncertainties-arcsec :max  0.33 :rms  0.29 :avg  0.29)
(reference-point :etrs (3837961.573 449582.374 5057364.221)
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
    #2A((-0.11959555954023805 -0.791953940633127 0.5987535870906487)
        (0.9928226936782502 -0.0954186184715623 0.07209983471854887)
        (3.24918607300384e-5 0.6030789692607761 0.7976814876751491)))
