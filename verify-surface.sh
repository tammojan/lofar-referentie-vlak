#!/bin/bash
ROOT=$HOME/Lofar/lofar-referentie-vlak
STATCOR=$ROOT/statcor
CONTOURMAP_PY=$ROOT/contourmap.py

PDFOPEN=open

case `uname` in
    Darwin) PDFOPEN=open;;
    Linux)  PDFOPEN=okular;;
esac

LAND_SURVEYOR= #-l

input_name=$1
prefix=$2
output_solution_name=$prefix-solution.lisp
core_solution_name=$prefix-core-solution.lisp

antenna_stake_files=""

if test "$#" -ge "3"; then
    core_solution_name=$3
fi
if test "$#" -ge "4"; then
    antenna_stake_files="$4 $5 $6 $7 $8 $9"
fi

surface_pqr_name=$prefix-surface-pqr.csv

PWD=`pwd`
dirname=`basename $PWD`
station=`basename $PWD |sed -e 's/-/ - /g'|sed -e 's/cs/CS/g'|sed -e 's/rs/RS/g'`
code=`echo $dirname|sed -e 's/\(?????\)-.*/\1/g'`

if test "$#" -lt "3"; then

    $STATCOR fit-plane -u 0.015 $LAND_SURVEYOR -o $output_solution_name  $input_name

    etrs_centre=`grep '(reference-point :etrs' $output_solution_name|sed -e 's/(reference-point :etrs//g'`
    echo $etrs_centre
    cat > $core_solution_name <<EOF
(normal-vector (0.598753 0.072099 0.797682))
(reference-point :etrs ${etrs_centre}
                 :station-pqr (0.0 0.0 0.0))
(station-pqr-to-etrs-matrix
  #2A((-0.11959510541518863d0 -0.7919544517060547d0 0.5987530018160178d0)
      (0.9928227483621251d0 -0.09541868004821492d0 0.07209900021867627d0)
      (3.3096921454367496d-5 0.6030782883845335d0 0.7976820024193695d0)))

EOF
fi


echo "$STATCOR etrs-to-pqr $LAND_SURVEYOR -o $surface_pqr_name $core_solution_name $input_name"
$STATCOR etrs-to-pqr $LAND_SURVEYOR -o $surface_pqr_name $core_solution_name $input_name
ln -sf $CONTOURMAP_PY ./contourmap.py

cat > height-map-script.py <<EOF
from pylab import *
from contourmap import *

antenna_stakes=[read_pqr(name, ',') for name in '$antenna_stake_files'.split()]
if '$antenna_stake_files' is not '':
    antn=concatenate([n for n,p,q,r in antenna_stakes])
    antp=concatenate([p for n,p,q,r in antenna_stakes])
    antq=concatenate([q for n,p,q,r in antenna_stakes])
    antr=concatenate([r for n,p,q,r in antenna_stakes])
    antenna_positions=[antn, antp, antq, antr]
else:
    antenna_positions=[]
    pass

n,p,q,r = read_pqr('$surface_pqr_name')

ar = array(r)
sigma=ar.std()
ar_mean=ar.mean()
for i in range(10):
    new_sigma=ar[abs(ar-ar_mean)< 3*sigma].std()
    ar_mean=ar[abs(ar-ar_mean)< 3*sigma].mean()
    sigma=new_sigma
    pass
fig=plot_height_map_stats(p,q,r, '$prefix',standard_deviations=[int(sigma*1000 +0.5)/1000.0], antenna_positions=antenna_positions,resolution_m=2.0, mean_of_good_points=ar_mean,vmax=0.15)
savefig('$prefix-height-map-stats.pdf',dpi=300, format='pdf',orientation='landscape')

fig=plot_height_map(p,q,r, '$prefix',standard_deviations=[int(sigma*1000 +0.5)/1000.0], antenna_positions=antenna_positions,resolution_m=2.0,vmax=0.15)
savefig('$prefix-height-map.pdf',dpi=300, format='pdf',orientation='landscape')
EOF


python2 height-map-script.py
echo '------------------------------------------------------------'
echo "              $station"
echo
echo 'De volgende punten wijken teveel af van het bedoelde vlak:'
echo
grep -e ';-\?0\.[123456789]..$' $prefix-surface-pqr.csv
grep -e ';-\?0\.0[3456789].$' $prefix-surface-pqr.csv
echo
echo '------------------------------------------------------------'

$PDFOPEN $prefix-height-map.pdf &

$PDFOPEN $prefix-height-map-stats.pdf&
