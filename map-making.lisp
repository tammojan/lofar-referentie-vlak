(in-package :lofar-reference-plane)


#||

\section{Map making}

Once a field has been measured, we would like to produce a map of the
field indicating the locations and names  of the stakes, the
local geographic north, and some other information, such as the height
of the surface with respect to the heads of the stakes.


Stations are typically less than $150\times90$~m in size. Maps at a
scale of 1:1000 therefore seem appropriate. The area of the map draing
will be $150\times150$~mm. At the top of the map is some other
information, such as the name of the station and the scale of the map.

||#

(defun make-map (station-name stake-list output-pathname &key
                 (scale 1/2000)
                 (width-mm 150)
                 (height-mm 150)
                 (stake-size-mm 0.5)
                 laser-stake-name
                 contour-stake-name-list
                 distance-pairs)
  "Stake list contains plists with at least the fields :STATION-P,
:STATION-Q, and :STATION_R, and an optional :NAME and :COMMENT"
  (labels ((field (field)
             (lambda (x) (getf x field)))
           (reduce-stakes (function field)
             (reduce function stake-list :key (field field)))
           (mm (mm) (* (/ mm 25.4) 72)))
    (let* ((min-p-m (reduce-stakes #'min :station-p))
           (max-p-m (reduce-stakes #'max :station-p))
           (min-q-m (reduce-stakes #'min :station-q))
           (max-q-m (reduce-stakes #'max :station-q))
           (p0 (average (list min-p-m max-p-m)))
           (q0 (average (list min-q-m max-q-m))))
      (labels ((xy-from-pq (p q)
                 (list (* scale 1000 (- p p0))
                       (* scale 1000 (- q q0))))

               (string-size (string font font-size)
                 (if (string= string "")
                     0d0
                     (let ((char-dimensions (map 'list
                                                 (lambda (char)
                                                   (multiple-value-list
                                                    (pdf:get-char-size char font font-size)))
                                                 string)))
                       (values (reduce #'+ char-dimensions :key #'first)
                               (reduce #'max char-dimensions :key #'second)
                               ))))

               (draw-stake (name p-m q-m r-m comment font font-size)
                 (print name)
                 (pdf:set-line-width 0.5)
                 (pdf:set-gray-stroke 0.0)
                 (destructuring-bind (x y) (xy-from-pq p-m q-m)
                   (pdf:set-rgb-fill 0 0 0)
                   (pdf:circle x y stake-size-mm)
                   (pdf:close-fill-and-stroke)
                   (when (and name laser-stake-name (eql name laser-stake-name))
                     (pdf:circle x y (* 3 stake-size-mm))
                     (pdf:close-and-stroke))
                   (pdf:in-text-mode
                     (let ((text (if (or (not comment) (string= comment ""))
                                     (format nil "~a" name)
                                     (format nil "~a(~a)" name comment))))
                       (multiple-value-bind (width height) (string-size text font font-size)
                         (pdf:set-font font font-size)
                         (pdf:move-text (- x (/ width 2.0))
                                        (+ y (* 4 stake-size-mm)))
                         (pdf:draw-text text))))
                   (pdf:in-text-mode
                     (when (numberp r-m)
                       (let ((text (format nil "~@d" (round (* 1000 r-m)))))
                         (multiple-value-bind (width height) (string-size text font font-size)
                           (pdf:set-font font font-size)
                           (pdf:move-text (- x (/ width 2.0))
                                          (- y 
                                             (* 2 stake-size-mm)
                                             (* 1.33 height)))
                           (pdf:draw-text text)))))))

               (draw-contour (contour-names stake-list)
                 (pdf:set-line-width 0.5)
                 (pdf:set-gray-stroke 0.0)
                 (pdf:set-dash-pattern '(3 2 1 2) 0)
                 (let ((positions (mapcar (lambda (name)
                                            (let ((row (find-if (lambda (x) (eql (getf x :name) name)) stake-list)))
                                              (xy-from-pq (getf row :station-p) (getf row :station-q))))
                                          contour-names)))
                   (apply #'pdf:move-to (first positions))
                   (dolist (position (rest positions))
                     (apply #'pdf:line-to position))
                   (pdf:close-and-stroke)))
               
               (vector- (a &rest others)
                 (apply #'map 'vector #'- a others))

               (draw-distance-table (name-pairs stake-list x0 y0 font font-size)
                 (let ((positions (print (apply #'mapcar (lambda (name &rest positions)
                                                           (list name (apply #'vector positions)))
                                                (mapcar (lambda (name)
                                                          (mapcar (field name) stake-list))
                                                        '(:name :station-p :station-q))))))
                   (loop :for (point1 point2) :in name-pairs
                      :for row :from 0
                      :do
                      (pdf:in-text-mode
                        (pdf:set-font font font-size)
                        (pdf:move-text x0 (- y0 (* row font-size)))
                        (pdf:draw-text 
                         (format nil "~a-~6a" point1 point2)))
                      (pdf:in-text-mode
                        (pdf:set-font font font-size)
                        (let ((text (format nil "~5,1f m"
                                            (vector-abs
                                             (vector- (second (assoc point1 positions))
                                                      (second (assoc point2 positions)))))))
                          (pdf:move-text (- (+ x0 (string-size "P_11-P_99  123.5 m" font font-size)) (string-size text font font-size))
                                         (- y0 (* row font-size)))
                          (pdf:draw-text text)))))))


        (pdf:with-document ()
          (pdf:with-page ()
            (let ((helvetica (make-instance 'pdf:font)))
              (pdf:add-font-to-page helvetica)
              (pdf:translate (mm (/ 210d0 2)) (mm (* 297 0.6)))
              (pdf:scale (mm 1) (mm 1))
              (pdf:in-text-mode
                (pdf:set-font helvetica 10.0)
                (pdf:move-text -75 92)
                (pdf:draw-text (format nil "STATION ~a" station-name)))
              (pdf:in-text-mode
                (pdf:set-font helvetica 6.0)
                (pdf:move-text -75 85)
                (pdf:draw-text "Scale 1:2000"))

              (pdf:set-line-width 0.5)
              (pdf:move-to -75 75)(pdf:line-to 75 75)(pdf:line-to 75 -75)(pdf:line-to -75 -75)
              (pdf:close-and-stroke)
              (apply #'mapcar (lambda (name p q r comment)
                                (draw-stake name p q r comment helvetica 3.0))
                     (mapcar #'(lambda (field) (mapcar (field field) stake-list))
                              '(:name :station-p :station-q :station-r :comment)))
              (when contour-stake-name-list
                (draw-contour contour-stake-name-list stake-list))
              (when distance-pairs
                (draw-distance-table distance-pairs stake-list -75 -85 helvetica 6.0))
              ))
          (with-open-file (stream output-pathname  
                                  :direction :output 
                                  :if-exists :supersede
                                  :element-type :default)
            
            (pdf:write-document stream)))))))
