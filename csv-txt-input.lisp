(in-package :lofar-reference-plane)

#||
\section{Reading the data}
\label{sec:reading-the-data}

During the analysis, each line containing data is stored in memory in
an object of the class \cl{position-measurement}.

||#
(defclass position-measurement ()
  ((position-id   :initform nil :initarg :position-id :reader position-id)
   (etrs-x      :initform nil :initarg :etrs-x :reader etrs-x)
   (etrs-y      :initform nil :initarg :etrs-y :reader etrs-y)
   (etrs-z      :initform nil :initarg :etrs-z :reader etrs-z)
   (uncertainty :initform nil :initarg :uncertainty :reader uncertainty)))

(define-class-printer position-measurement)
#||

These objects are created with the function \cl{make-position-measurement},
which takes a list containing the numbers from one line in the data
file. If a line is incomplete, that is, it contains fewer numbers than
required, \cl{make-position-measurement} will fill the \cl{position-measurement}
from front to back until it runs out of numbers.

||#
(deftest test-make-position-measurement ()
  (let ((mp (make-position-measurement 
             '(10072 3826526.58982d0 460857.31790446d0 5064948.2110779d0 0.04d0))))
    (assert-eql (position-id mp) 10072)
    (assert-approx= (etrs-y mp) 460857.31790446d0)
    (assert-approx= (uncertainty mp) 0.04d0))
  (let ((mp (make-position-measurement 
             '(p5 3826526.58982d0 460857.31790446d0) :uncertainty 1d0)))
    (assert-eql (position-id mp) 'p5)
    (assert-approx= (etrs-y mp) 460857.31790446d0)
    (assert-approx= (etrs-z mp) nil)
    (assert-approx= (uncertainty mp) 1d0)))

(defun make-position-measurement (ecef-list &rest defaults)
  (apply #'make-instance 'position-measurement
         (append
          (zip '(:position-id :etrs-x :etrs-y :etrs-z :uncertainty)
               ecef-list)
          defaults)))
#||

Some functions, such as \cl{fit-plane}, require that all fields are
filled. \cl{validate-position-measurement} verifies this and throws an
error in case of trouble.

||#
(deftest test-validate-position-measurement ()
  (assert-error (validate-position-measurement
                 (make-position-measurement 
                  '(p5 3826526.58982d0 460857.31790446d0) :uncertainty 1d0))
                error)
  (assert-eql (class-name
               (class-of (validate-position-measurement
                          (make-position-measurement 
                           '(p5 3826526.58982d0 460857.31790446d0 10) :uncertainty 1d0))))
              'position-measurement))

(defun validate-position-measurement (position-measurement)
  (loop :for slot :in (class-slot-symbols 'position-measurement)
     :when (null (slot-value position-measurement slot)) :do
     (error "Parameter ~a was not in the input data, nor was a default given." slot))
  position-measurement)
#||

Comments in the file start with a \# mark and end at the end of the
line. They must be stripped before a line is parsed. Sometimes, a
certain character is used as a separator between fields. In order not
to confuse the reader, it should be replaced by a space.

||#
(deftest test-strip-comments ()
  (assert-string= (strip-comments "")                  "")
  (assert-string= (strip-comments "hi there    ")      "hi there    ")
  (assert-string= (strip-comments "#asdjfsldfgjbh")    "")
  (assert-string= (strip-comments "asdf  #  sfg # fg") "asdf  "))

(defun strip-comments (string)
  (subseq string 0 (position #\# string)))

(deftest test-remove-separator ()
  (assert-string= (remove-separator "p5;120;125;-12.18" #\;) "p5 120 125 -12.18")
  (assert-string= (remove-separator "p5,120,125;-12.18" #\; #\,) "p5 120 125 -12.18"))

(defun remove-separator (string &rest separators)
  (if separators
      (apply #'remove-separator
             (substitute #\SPACE (first separators) string)
             (rest separators))
      string))
#||

Once stripped of comments, separators are replaced by spaces, and the
line is parsed by the Lisp reader. Note that by doing this, we do
\emph{not} implement a formal CSV reader. For example, it cannot
handle empty fields properly. The main thing is that we do not choke
on files that happen to be in CSV format with commas or semicolons as
separators. Because we want to represent the row as a list of numbers
that can be fed to \cl{make-position-measurement}, we enclose the line
in parentheses after stripping the comments. The dynamic variable
\cl{*read-default-float-format*} is set to \cl{'double-float} in order
to force the reader to convert the floating point numbers to double
precision floats.

||#
(deftest test-parse-line ()
  (assert-eql (parse-line 
               "#Punt ETRS X ETRS Y  ETRS Z   RD-X RD-Y RD-Z Error estimate")
              '())
  (assert-equal (parse-line 
                 "Punt ETRS X ETRS Y  ETRS Z   RD-X RD-Y RD-Z Error estimate")
                '(PUNT ETRS X ETRS Y ETRS Z RD-X RD-Y RD-Z ERROR ESTIMATE))
  (assert-equal (parse-line 
                 "10025 3826662.564524   461164.45487807   5064818.3502321   
                   254858.18782369   548460.04550816   8.1362529658367   0.04")
                '(10025 3826662.564524d0 461164.45487807d0 5064818.3502321d0
                  254858.18782369d0 548460.04550816d0 8.1362529658367d0 0.04d0)))

(defun parse-line (string)
  (let ((*read-default-float-format* 'double-float))
    (read-from-string
     (concatenate 'string "(" (remove-separator (strip-comments string) #\; #\, #\Tab) ")"))))
#||

Given a file name, the entire file is parsed by collecting all
non-empty results of the line parser. If a line consists of only
symbols, it is assumed to be the header of a CSV file. It will be
ignored. One can optionally provide default values for the
\cl{position-measurement}s that will be used if a line does not
contain an entry for that value.

||#
(deftest test-parse-file ()
  (let ((mp-list (parse-file *raw-position-file-name-csv* :uncertainty 0.06d0)))
    (assert-eql     (length mp-list)                     76)
    (assert-eql     (position-id (first mp-list))          10000)
    (assert-eql     (position-id (first (last mp-list)))   10075)
    (assert-approx= (uncertainty (first (last mp-list))) 0.06d0)
    (assert-approx= (etrs-z (elt mp-list 17))            5064785.0413195d0)))

(defun parse-stream (stream &rest default-values)
  (loop :for line = (read-line stream nil nil)
       :while line
       :for   parsed-line = (parse-line line)
       :when  (and parsed-line
                   (not (or (every #'symbolp parsed-line)
                            (every #'stringp parsed-line))))
       :collect (apply #'make-position-measurement 
                       (list* parsed-line default-values))))

(defun parse-file (pathname &rest default-values)
  (with-open-file (stream pathname :direction :input)
    (apply #'parse-stream stream default-values)))
#||

It is sometimes also useful to operate the other way around. That is,
given a \cl{position-measurement}, write a CSV line with the fields
separated by semicolons.

||#
(deftest test-write-csv-line ()
  (assert-string= (as-csv-string (make-position-measurement
                                  '(P0 3.14159255637d0 12.15d0 3174456.2535466d0 0.08d0)))
                  "P0;3.14159255637;12.15;3174456.2535466;0.08"))

(defmethod write-csv-line ((object position-measurement) stream)
  (format stream "~a;~f;~f;~f;~f"
          (position-id object)
          (etrs-x object) (etrs-y object) (etrs-z object)
          (uncertainty object)))

(defun as-csv-string (object)
  (with-output-to-string (stream)
    (write-csv-line object stream)))
#||

It is also useful to read general CSV files as lists of PLISTS.

||#


(deftest test-parse-csv-header-line ()
  (assert-equalp (parse-csv-header-line "NAME,\"STATION-P\",,STATION-Q,STATION-R")
                 '(:name :station-p nil :station-q :station-r)))


(defun parse-csv-header-line (line &key (separator #\,))
  (let ((*read-default-float-format* 'double-float))
    (mapcar (lambda (item)
              (if (string= item "")
                  nil
                  (intern (string-upcase (format nil "~a" (read-from-string item))) :keyword)))
            (cl-utilities:split-sequence separator line))))



(deftest test-parse-csv-line ()
  (assert-equalp (parse-csv-line "\"CLBA\";3827908.969;459843.240;5064012.805;;14.081;-0.083"
                                 '(:name :etrs-x :etrs-y :etrs-z :station-p :station-q :station-r)
                                 :separator #\;)
                 '(:name "CLBA" 
                   :etrs-x 3827908.969d0 :etrs-y 459843.240d0 :etrs-z 5064012.805d0
                   :station-p nil :station-q 14.081d0 :station-r -0.083d0))
  (assert-equalp (parse-csv-line "\"CLBA\";3827908.969;459843.240;5064012.805;;14.081;-0.083"
                                 '(:name :etrs-x :etrs-y :etrs-z nil :station-q :station-r)
                                 :separator #\;)
                 '(:name "CLBA" 
                   :etrs-x 3827908.969d0 :etrs-y 459843.240d0 :etrs-z 5064012.805d0
                   nil nil :station-q 14.081d0 :station-r -0.083d0)))
                        
                        
(defun parse-csv-line (line header &key (separator #\,))
  (let ((*read-default-float-format* 'double-float))
    (zip header
         (mapcar (lambda (item)
                   (let ((stripped (string-trim '(#\SPACE #\TAB) item))) 
                     (if (string= stripped "")
                         nil
                         (read-from-string stripped))))
                 (cl-utilities:split-sequence separator line)))))

(defun parse-csv-stream (stream &key (separator #\,))
  (let* ((lines (read-lines stream))
         (header (parse-csv-header-line (first lines) :separator separator)))
    (mapcar (lambda (line)
              (parse-csv-line line header :separator separator))
            (rest lines))))


(defun parse-csv-file (pathname &key (separator #\,))
  (with-open-file (stream pathname :direction :input)
    (parse-csv-stream stream :separator separator)))




(defun read-etrs-lon-lat (filename &rest default-values)
  (let ((records (parse-csv-file filename)))
    (loop :for record :in records 
       :collect 
                 (append (list (getf record :name))
                         (to-list
                          (xyz-from-geographic (apply #'rad-from-dms (getf record :etrs-lon))
                                               (apply #'rad-from-dms (getf record :etrs-lat))
                                               (getf record :etrs-h)))
                         (list (getf default-values :uncertainty))))))

(defun parse-azimuth-etrs-lon-lat (stream &rest default-values)
  (let ((*read-default-float-format* 'double-float))
    (mapcar (lambda(line)
              (let ((data
                     (read-from-string (concatenate 'string "(" (substitute #\Space #\, line) ")"))))
                (apply #'make-position-measurement
                       (list* (list* (first data)
                                     (to-list
                                      (xyz-from-geographic (rad-from-dms
                                                            (if (eql (elt data 8) 'E) '+ '-)
                                                            (elt data 5) (elt data 6) (elt data 7))
                                                           (rad-from-dms
                                                            (if (eql (elt data 4) 'N) '+ '-)
                                                            (elt data 1) (elt data 2) (elt data 3))
                                                           (elt data 9))))
                              default-values))))
                                                           
            (read-lines stream))))

(defun parse-azimuth-etrs-lon-lat-deg (stream &rest default-values)
  (let ((*read-default-float-format* 'double-float))
    (mapcar (lambda(line)
              (let ((data
                     (read-from-string (concatenate 'string "(" (substitute #\Space #\, line) ")"))))
                (apply #'make-position-measurement
                       (list* (list* (first data)
                                     (to-list
                                      (xyz-from-geographic (rad-from-deg (elt data 2))
                                                           (rad-from-deg (elt data 1))
                                                           (elt data 3))))
                              default-values))))
                                                           
            (read-lines stream))))

(defun parse-azimuth-etrs-lon-lat-file (filename &rest default-values)
  (with-open-file (stream filename :direction :input)
    (apply #'parse-azimuth-etrs-lon-lat-deg (list* stream default-values))))
