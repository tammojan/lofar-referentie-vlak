#!/bin/bash

station="|`echo $1|sed -e 's/\([:alnum:]*\)-.*/\1/g'|sed -e's/r/R/g'|sed -e 's/c/C/g'|sed -e's/s/S/g'`"

if test "$#" == "3"; then
    lba_pqr=$1
    hba_pqr=$2
    solution=$3
    
    declare -a M0
    M0=( `cat $lba_pqr|grep M0|sed -e's/,/ /g'` )
    echo ${M0[1]}
    
    M0P=`echo "${M0[1]} + 0.06"|bc`
    M0Q=`echo "${M0[2]} + 0.06"|bc`
    M0R=${M0[3]}
    
    
    
    echo $station`statcor pqr-to-etrs $solution <<- EOF |grep LBA  |sed -e's/;/|/g'
NAME;STATION-P;STATION-Q;STATION-R
LBA;${M0P};${M0Q};${M0R}
EOF
    `|sed -E -e "s/(\|[:alnum:]+\|[0-9.+-]+\|[0-9.+-]+\|[0-9.+-]+\|).*/\1/g"

    statcor pqr-to-etrs $solution $hba_pqr |grep A18 |sed -e's/;/|/g'|sed -e "s:A18:${station}HBA0:g"|sed -E -e 's/(\|[:alnum:]+\|[0-9.+-]+\|[0-9.+-]+\|[0-9.+-]+\|).*/\1/g'
    statcor pqr-to-etrs $solution $hba_pqr |grep A55 |sed -e's/;/|/g'|sed -e "s:A55:${station}HBA1:g"|sed -E -e 's/(\|[:alnum:]+\|[0-9.+-]+\|[0-9.+-]+\|[0-9.+-]+\|).*/\1/g'


    echo `statcor pqr-to-etrs $solution <<- EOF |grep LBA
NAME;STATION-P;STATION-Q;STATION-R
CLBA;${M0P};${M0Q};${M0R}
EOF
    `|sed -E -e "s/(\|[:alnum:]+\|[0-9.+-]+\|[0-9.+-]+\|[0-9.+-]+\|).*/\1/g"|sed -e 's/;/,/g'
    statcor pqr-to-etrs $solution $hba_pqr |grep A18 |sed -e "s:A18:CHBA0:g"|sed -E -e 's/(\|[:alnum:]+\|[0-9.+-]+\|[0-9.+-]+\|[0-9.+-]+\|).*/\1/g'|sed -e 's/;/,/g'
    statcor pqr-to-etrs $solution $hba_pqr |grep A55 |sed -e "s:A55:CHBA1:g"|sed -E -e 's/(\|[:alnum:]+\|[0-9.+-]+\|[0-9.+-]+\|[0-9.+-]+\|).*/\1/g'|sed -e 's/;/,/g'


elif test "$#" == "2"; then
    lba_core=$1
    hba_core=$2

    
    echo $station`statcor pqr-to-etrs $lba_core <<- EOF |grep LBA  |sed -e's/;/|/g'
NAME;STATION-P;STATION-Q;STATION-R
LBA;0.0;0.0;0.0
EOF
    `|sed -E -e 's/(\|[:alnum:]+\|[0-9.+-]+\|[0-9+-.]+\|[0-9.+-]+\|).*/\1/g'
    echo $station`statcor pqr-to-etrs $hba_core <<- EOF |grep HBA  |sed -e's/;/|/g'
NAME;STATION-P;STATION-Q;STATION-R
HBA;0.0;0.0;0.0
EOF
    `|sed -E -e 's/(\|[:alnum:]+\|[0-9.+-]+\|[0-9+-.]+\|[0-9.+-]+\|).*/\1/g'
fi

