(in-package :lofar-reference-plane)

(defparameter *rs307-hba-centre-etrs-ellipsoid*
  (list (rad-from-dms '+  6 40 55.03926)
        (rad-from-dms '+ 52 48 10.43472)
        58.643d0))
  
(defparameter *rs307-hba-centre-etrs*
  #(3837961.7227078364d0 449631.52574780205d0 5057359.707621691d0))

(defparameter *rs307-lba-centre-etrs-ellipsoid* 
  (list (rad-from-dms '+  6 40 51.40248)
        (rad-from-dms '+ 52 48 11.59098)
        58.643d0))

(defparameter *rs307-lba-centre-etrs*
  #(3837941.372114338d0 449560.54391393013d0 5057381.315935495d0))

(defparameter *rs307-centre-etrs*
  (map 'vector #'(lambda (x y) (/ (+ x y) 2d0))
       *rs307-lba-centre-etrs*
       *rs307-hba-centre-etrs*))
;;========================
;; RS307 centre ETRS:
;;(X: 3837951.547411087d0
;; Y:  449596.0348308661d0
;; Z: 5057370.511778593d0)
;;========================

(defparameter *rs307-dates*
  '((2008 9 8 11 0 0)
    (2008 9 8 14 0 0)))

(defparameter *rs307-personnel*
  '("Hans König (Azimuth)"
    "Henk Smit (HAK)"
    "Hans van der Marel (ASTRON)"
    "Michiel Brentjens (ASTRON)"
    "Arie Huijgen (ASTRON)"))

;; NAP height of p0: 16:80

(defparameter *rs307-reference-plane-etrs*
  '((p0 3837951.547411087d0  449596.0348308661d0  5057370.511778593d0)
    (a1 3837893.127d0        449551.008d0         5057417.817d0      )
    (p1 3837893.4211447514d0 449551.0434195176d0  5057418.208871061d0)
    (a2 3837927.158d0        449517.577d0         5057395.646d0      )
    (p2 3837927.2842459953d0 449517.59220194473d0 5057395.814189818d0)
    (a3 3837958.235d0        449502.764d0         5057373.539d0      )
    (p3 3837958.418074529d0  449502.7860449676d0  5057373.782898998d0)
    (a4 3838019.592d0        449614.453d0         5057317.478d0      )
    (p4 3838019.732262234d0  449614.4698897138d0  5057317.664862796d0)
    (a5 3837957.630d0        449673.247d0         5057358.437d0      )
    (p5 3837957.8832395864d0 449673.27749391145d0 5057358.774375612d0)
    (au 3837992.134d0        449635.534d0         5057336.211d0      )
    (pu 3837992.260888457d0  449635.5492793069d0  5057336.38004573d0 )
    (as 3837991.855d0        449568.217d0         5057342.387d0      )
    (ps 3837992.0382050313d0 449568.23906068207d0 5057342.631072858d0)))

(defparameter *laser* 
  '(:west-to-east   :x  -0.024 '%
    :south-to-north :y +0.25 '%) 
  "laser staat bij P3, geijkt met behulp van P2 en P4. controle bij P1
   OK, PU 8mm op -0.020%, P4 3mm te hoog op -0.020%, vandaar -0.024%
en PS binnen toleranties")
  